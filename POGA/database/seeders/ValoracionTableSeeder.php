<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Valoracion;

class ValoracionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = storage_path() . "/json/crearValoraciones.json";
        $json = file_get_contents($path);
        $arrays = json_decode($json, true);
        foreach ($arrays as $array) {
            Valoracion::create([
                'id_user' => $array['id_user'],
                'id_receta' => $array['id_receta'],
                'puntuacion' => $array['puntuacion'],
                'favorito' => $array['favorito'],
                'opinion' => $array['opinion'],
            ]);
        }
        $path = storage_path() . "/json/valoraciones.json";
        $json = file_get_contents($path);
        $arrays = json_decode($json, true);
        foreach ($arrays as $array) {
            Valoracion::create([
                'id_user' => $array['id_user'],
                'id_receta' => $array['id_receta'],
                'puntuacion' => $array['puntuacion'],
                'favorito' => $array['favorito'],
                'opinion' => $array['opinion'],
            ]);
        }
    }
}
