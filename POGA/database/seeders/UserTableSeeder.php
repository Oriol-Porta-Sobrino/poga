<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use \Datetime;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = storage_path() . "/json/usuarios.json";
        $json = file_get_contents($path);
        $arrays = json_decode($json, true);
        foreach ($arrays as $array) {
            $fechaString = strtotime($array['nacimiento']);
            $fecha = date('Y-m-d', $fechaString);
            User::create([
                'name' => $array['name'],
                'apellido' => $array['apellido'],
                'genero' => $array['genero'],
                'pais' => $array['pais'],
                'ciudad' => $array['ciudad'],
                'telefono' => $array['telefono'],
                'nacimiento' => $fecha,
                'email' => $array['email'],
                'password' => Hash::make($array['password']),
                'fotografia' => $array['fotografia'],
                ]);
        }
    }
}
