<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Receta;

class RecetaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {      
        $path = storage_path() . "/json/prueba.json";
        $json = file_get_contents($path);
        $arrays = json_decode($json, true);
        foreach ($arrays as $array) {
            Receta::create([
                'nombre' => $array['nombre'],
                'origen' => $array['origen'],
                'dificultad' => $array['dificultad'],
                'descripcion' => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae reprehenderit nisi tenetur ipsa quasi non odio officia enim aut laborum molestiae necessitatibus ipsam labore sint, sed accusamus. Perferendis, ipsam ex?",
                'tipo' => $array['tipo'],
                'duracion' => $array['duracion'],
                'personas' => $array['personas'],
                'id_usuario' => $array['id_usuario'],
                'fotografia' => $array['fotografia'],
            ]);
        }      
    }
}
