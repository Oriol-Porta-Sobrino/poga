<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecomendacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('recomendacions', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_receta')->unsigned();
			$table->foreign('id_receta')->references('id')->on('recetas')->onUpdate('cascade')->onDelete('restrict');
			$table->bigInteger('id_usuario')->unsigned();
			$table->foreign('id_usuario')->references('id')->on('users')->onUpdate('cascade')->onDelete('restrict');
            $table->tinyInteger('actuales')->default(1);	
            $table->tinyInteger('top')->unsigned();
            $table->timestamps();
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('recomendacions');
        Schema::enableForeignKeyConstraints();
    }
}
