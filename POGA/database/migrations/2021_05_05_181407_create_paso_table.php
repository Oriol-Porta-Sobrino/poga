<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePasoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('pasos', function (Blueprint $table) {
            $table->id();
            $table->integer('numero');
            $table->string('descripcion');
            $table->bigInteger('id_receta')->unsigned();
            $table->foreign('id_receta')->references('id')->on('recetas')->onUpdate('cascade')->onDelete('restrict');
            $table->timestamps();
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('pasos');
        Schema::enableForeignKeyConstraints();
    }
}
