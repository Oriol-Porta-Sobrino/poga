<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateValoracionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('valoracions', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_receta')->unsigned();
			$table->foreign('id_receta')->references('id')->on('recetas')->onUpdate('cascade')->onDelete('restrict');
			$table->bigInteger('id_user')->unsigned();
			$table->foreign('id_user')->references('id')->on('users')->onUpdate('cascade')->onDelete('restrict');	
            $table->tinyInteger('puntuacion')->unsigned()->nullable();
            $table->tinyInteger('favorito')->unsigned()->nullable();
            $table->string('opinion')->nullable();
            $table->timestamps();
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('valoracions');
        Schema::enableForeignKeyConstraints();
    }
}
