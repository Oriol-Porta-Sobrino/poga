<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLlevaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('lleva', function (Blueprint $table) {
            $table->primary(['idReceta', 'idIngrediente']);
            $table->bigInteger('idReceta')->unsigned();
			$table->foreign('idReceta')->references('id')->on('receta')->onUpdate('cascade')->onDelete('restrict');
			$table->bigInteger('idIngrediente')->unsigned();
			$table->foreign('idIngrediente')->references('id')->on('ingrediente')->onUpdate('cascade')->onDelete('restrict');	
            $table->timestamps();
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('lleva');
        Schema::enableForeignKeyConstraints();
    }
}
