<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIngredienteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('ingredientes', function (Blueprint $table) {
            $table->id();
            $table->tinyText('nombre');
            $table->tinyText('cantidad');
            $table->bigInteger('id_receta')->unsigned();
            $table->foreign('id_receta')->references('id')->on('recetas')->onUpdate('cascade')->onDelete('restrict');
            $table->timestamps();
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('ingredientes');
        Schema::enableForeignKeyConstraints();
    }
}
