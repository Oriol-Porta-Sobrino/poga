<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('recetas', function (Blueprint $table) {
            $table->id();
            $table->string('nombre', 100);
            $table->enum('origen', ['Europea', 'Asiatica', 'Norteamericana', 'Sudamericana', 'Oceanica', 'Africana']);
            $table->enum('dificultad', ['Pinche', 'Chef', 'Masterchef']);
            $table->enum('tipo', ['Aperitivos y tapas', 'Arroces y cereales', 'Aves', 'Carnes', 'Ensaladas', 'Guisos y potajes', 'Legumbres', 'Mariscos', 'Pescados', 'Postres', 'Pasta', 'Salsa', 'Sopas y cremas', 'Verduras']);
            $table->text('descripcion')->nullable();
            $table->integer('duracion')->unsigned();
            $table->integer('personas')->unsigned();
            $table->string('fotografia')->nullable();
            $table->bigInteger('id_usuario')->unsigned();
            $table->foreign('id_usuario')->references('id')->on('users')->onUpdate('cascade')->onDelete('restrict');
            $table->timestamps();
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('recetas');
        Schema::enableForeignKeyConstraints();
    }
}
