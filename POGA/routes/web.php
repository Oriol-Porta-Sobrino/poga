<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TemporalController;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\RecetaController;
use App\Http\Controllers\ValoracionController;
use App\Http\Controllers\PrincipalController;
use App\Http\Controllers\AjaxController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::group(['middleware' => 'auth'], function() {
    Route::get('/insertarRecetaForm',[RecetaController::class, 'insertRecetaRedirect']);
    Route::post('/insertarReceta',[RecetaController::class, 'insertReceta'])->name('anadirReceta');
    Route::get('/updateRecetaForm/{id}', [RecetaController::class, 'updateRecetaRedirect']);
    // Route::post('/updateReceipe', [RecetaController::class, 'updateReceta'])->name('updateReceta');
    Route::post('/updateReceta', [RecetaController::class, 'updateReceta'])->name('updateReceta');
    Route::get('/updateUsuarioForm', [UsuarioController::class, 'updateUsuarioRedirect']);
    Route::post('/updateUsuario', [UsuarioController::class, 'updateUsuario'])->name('updateUsuario');
    Route::get('/insertReceta', [RecetaController::class, 'pruebaInsert']);
    Route::post('/borrarReceta', [RecetaController::class, 'deleteReceta'])->name('borrar');
    Route::post('/comentar', [ValoracionController::class, 'comentar'])->name('comentar');
});

Route::get('/verUsuario/{id}',[UsuarioController::class, 'verUsuario']);
Route::get('/buscar',[RecetaController::class, 'buscar'])->name('buscar');
Route::get('/filtrar',[RecetaController::class, 'filtrar'])->name('filtrar');
//Route::get('/recomendador',[PrincipalController::class, 'recomendador']);
Route::post('/puntuar',[ValoracionController::class, 'puntuar']);
Route::post('/like',[ValoracionController::class, 'like']);
//Route::get('/json',[RecetaController::class, 'probarJson']);
//Route::get('/check',[RecetaController::class, 'check']);
//Route::get('/crearValoracion',[RecetaController::class, 'crearValoracion']);
Route::get('/verReceta/{id}',[RecetaController::class, 'verReceta']);
Route::get('/',[PrincipalController::class, 'principal'])->name('home');
/**Route::get('/', function () {
    #return view('welcome');
    return view('index');
});**/

Route::get('/registrarse',[UsuarioController::class, 'registro']);
Route::get('/logear',[UsuarioController::class, 'login'])->name('logear');

/*Route::get('ajax',function() {
    return view('message');
 });*/
//Route::post('/getmsg',[AjaxController::class, 'index']);

Route::get('/home',[PrincipalController::class, 'principal']);
