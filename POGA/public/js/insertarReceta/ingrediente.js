function countIngrediente() {
    // Check to see if the counter has been initialized
    if ( typeof countIngrediente.counter == 'undefined' ) {
        // It has not... perform the initialization
        countIngrediente.counter = 0;
    }

    // Do something stupid to indicate the value
    return ++countIngrediente.counter;
}
function addIngrediente(){
    i = countIngrediente();
    // Number of inputs to create
    // Container <div> where dynamic content will be placed
    var container = document.getElementById("ingredientes");
    // Append a node with a random text
    container.appendChild(document.createTextNode("Ingrediente " + (i)));
    // Create an <input> element, set its type and name attributes
    var input = document.createElement("input");
    input.setAttribute("text", "ingrediente" + i);
    input.type = "text";
    input.name = "ingrediente" + i;
    input.className = "form-control";
    var cantidad = document.createElement("input");
    cantidad.type = "text";
    cantidad.name = "cantidad" + i;
    cantidad.className = "form-control";
    container.appendChild(input);
    container.appendChild(document.createTextNode("Cantidad"));
    container.appendChild(cantidad);
    // Append a line break
    container.appendChild(document.createElement("br"));
}