function getMessage() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });
    //e.preventDefault();
   $.ajax({
      type:'POST',
      url:'/getmsg',
      dataType: 'json',
      data: {'boton': 'hola'},
      success:function(data) {
          alert(data.msg);
         $("#msg").html(data.msg);
      },
    error: function (data) {
            console.log(data);
        }
   });
}

function like(i) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });
    //e.preventDefault();
    var src1 = "heart-plus";
    var src2 = "heart-remove";
    var src3 = "http://"+location.host +"/img/index_recipes/core-img/heart-plus-outline.svg";
    var src4 = "http://"+location.host +"/img/index_recipes/core-img/heart-remove.svg";

   $.ajax({
      type:'POST',
      url:'/like',
      dataType: 'json',
      data: {'receta': i},
      success:function(data) {
        if ($('.ico-like').attr("id") == src1)
            // alert(data.fav);
            $('.ico-like').attr({"src":src4, "id":src2});
        else
            // alert(data.fav);
            $('.ico-like').attr({"src":src3, "id":src1});
      },
    error: function (data) {
            console.log(data);
        }
   });
}


function puntuar(id, puntos) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type:'POST',
        url:'/puntuar',
        dataType: 'json',
        data: {'puntos': puntos,
                'receta': id},
        success:function(data) {
            alert("Has valorado esta receta con " + data.puntos + " estrellas");
        },
      error: function (data) {
              console.log(data);
          }
     });
}

