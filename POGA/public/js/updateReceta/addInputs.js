function countPaso() {
// Check to see if the counter has been initialized
    if ( typeof countPaso.counter == 'undefined' ) {
        // It has not... perform the initialization
        countPaso.counter = 0;
    }

    // Do something stupid to indicate the value
    return ++countPaso.counter;
}
function countIngrediente() {
// Check to see if the counter has been initialized
    if ( typeof countIngrediente.counter == 'undefined' ) {
        // It has not... perform the initialization
        countIngrediente.counter = 0;
    }

    // Do something stupid to indicate the value
    return ++countIngrediente.counter;
}
function addPaso(){
    i = countPaso();
    // Number of inputs to create
    // Container <div> where dynamic content will be placed
    var container = document.getElementById("pasos");
    // Append a node with a random text
    container.appendChild(document.createTextNode("Paso " + (i)));
    // Create an <input> element, set its type and name attributes
    var input = document.createElement("input");
    input.type = "text";
    input.name = "paso" + i;
    input.className = "form-control";
    /**var delButton = document.createElement("button");
    delButton.onclick = function() {
        input.remove();
        delButton.remove();
        countPaso.counter -= 1;
        var cambiar = document.getElementByName("paso1");
        cambiar.setAttribute('name', "sii");
    };
    delButton.textContent = "Borrar";**/
    container.appendChild(input);
    //container.appendChild(delButton);
    // Append a line break
    container.appendChild(document.createElement("br"));
}
function addIngrediente(){
    i = countIngrediente();
    // Number of inputs to create
    // Container <div> where dynamic content will be placed
    var container = document.getElementById("ingredientes");
    // Append a node with a random text
    container.appendChild(document.createTextNode("Ingrediente " + (i)));
    // Create an <input> element, set its type and name attributes
    var input = document.createElement("input");
    input.setAttribute("text", "ingrediente" + i);
    input.type = "text";
    input.name = "ingrediente" + i;
    input.className = "form-control";
    var cantidad = document.createElement("input");
    cantidad.type = "text";
    cantidad.name = "cantidad" + i;
    cantidad.className = "form-control";
    container.appendChild(input);
    container.appendChild(document.createTextNode("Cantidad"));
    container.appendChild(cantidad);
    // Append a line break
    container.appendChild(document.createElement("br"));
}
function carga(pasos, ingredientes) {

    var container = document.getElementById("pasos");
    for (var j = 0; j < pasos.length; j++) {
        i = countPaso();
        container.appendChild(document.createTextNode("Paso " + (i)));
        var input = document.createElement("input");
        input.type = "text";
        input.name = "paso" + i;
        input.className = "form-control";
        input.value = pasos[j]["descripcion"];
        container.appendChild(input);
        container.appendChild(document.createElement("br"));
    }
    var container = document.getElementById("ingredientes");
    for (var j = 0; j < ingredientes.length; j++) {
        i = countIngrediente();
        container.appendChild(document.createTextNode("Ingrediente " + (i)));
        var input = document.createElement("input");
        input.type = "text";
        input.name = "ingrediente" + i;
        input.className = "form-control";
        input.value = ingredientes[j]["nombre"];
        var cantidad = document.createElement("input");
        cantidad.type = "text";
        cantidad.name = "cantidad" + i;
        cantidad.className = "form-control";
        cantidad.value = ingredientes[j]['cantidad'];
        container.appendChild(input);
        container.appendChild(document.createTextNode("Cantidad"));
        container.appendChild(cantidad);
        container.appendChild(document.createElement("br"));
    }
}