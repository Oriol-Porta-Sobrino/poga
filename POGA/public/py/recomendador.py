import pandas as pd
import pymysql as py
import turicreate as tc

#Datos para conectarse en la BD
BD = 'poga'
usuario = 'poga'
contraseña = 'super3super3'

#Connexion a la BD
print("Empieza la connexion a la BD")
conexion = py.connect(user=usuario,passwd=contraseña,db=BD)
print("BD connectada")
#Necesitaremos el Cursor en el caso que queramos modificar, borrar o introducir datos
cursor = conexion.cursor()

#Consulta SQL para recoger los datos de las tablas
queryValoracion = "SELECT id_user, id_receta, puntuacion FROM valoracions;"

#Metemos los datos de las valoraciones en un DataFrame
print("se hacen las consultas para extraer los datos")
DataFrameValoracion = pd.read_sql(queryValoracion, conexion)

#Verificacion de que esta cogiendo los datos correctamente
print("Datos Extraidos:")
print(DataFrameValoracion.shape)
print(DataFrameValoracion)

#Pasando DataFrame a SFrame para entrenar el algoritmo
print("Se pasa el DataFrame a SFrame")
entrenamiento = tc.SFrame(DataFrameValoracion)

#Entrenamiento del algoritmo
print("Empieza a entrenar el algoritmo")
Sim_Usuario = tc.item_similarity_recommender.create(entrenamiento,user_id='id_user', item_id='id_receta', target='puntuacion', similarity_type='cosine')
print("Algortimo entrenado.")

#recomendacion de recetas
print("Empieza a recomendar el algoritmo")
Recomendado = Sim_Usuario.recommend(k=3)#num users || num recetas recomendadas

#miramos las recomendaciones para verificar
print("Algortimo recomendaciones:")
Recomendado.print_rows()

#Limpiamos la tabla de recomendaciones
cursor.execute("DELETE FROM poga.recomendacions")

#Metemos los datos recomendados en la BD Como lo que devuelve la recomendacion es un SFrame hay que volverlo a pasar a DataFrame
for index, row in Recomendado.to_dataframe().iterrows():
    cursor.execute("INSERT INTO poga.recomendacions (id_receta,id_usuario,top) values(%s,%s,%s)", ( row['id_receta'], row['id_user'], row['rank']))
conexion.commit()
cursor.close()
