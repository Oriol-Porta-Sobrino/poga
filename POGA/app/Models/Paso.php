<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paso extends Model
{
    use HasFactory;
    protected $fillable = ['numero', 'descripcion', 'id_receta'];

    public function Receta()
    {
        return $this->belongsTo(Receta::class);
    }
}
