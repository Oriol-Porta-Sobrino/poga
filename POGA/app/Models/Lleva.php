<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lleva extends Model
{
    use HasFactory;
    protected $primaryKey = ['idIngrediente', 'idReceta'];
    protected $fillable = ['idIngrediente', 'idReceta'];
}
