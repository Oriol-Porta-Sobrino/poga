<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Receta extends Model
{
    use HasFactory;
    protected $fillable = ['nombre', 'tipo', 'origen', 'dificultad', 'fotografia', 
    'duracion', 'personas', 'id_usuario', 'descripcion'];

    public function Usuario() 
    {
        return $this->belongsTo(User::class);
    }

    public function Paso()
    {
        return $this->hasMany(Paso::class);
    }

    public function Valoradas()
    {
        return $this->belongsToMany(User::class, 'Valoracion', 'idReceta', 'idUser');
    }

    public function Tiene()
    {
        return $this->hasMany(Ingrediente::class);
    }


}
