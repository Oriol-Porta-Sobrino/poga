<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Valoracion extends Model
{
    use HasFactory;
    protected $fillable = ['id_receta', 'id_user', 'puntuacion', 'opinion', 'favorito'];
}
