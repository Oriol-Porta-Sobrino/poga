<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ingrediente extends Model
{
    use HasFactory;
    protected $fillable = ['nombre', 'cantidad', 'id_receta'];

    /**public function EstaEn()
    {
        return $this->belongsToMany(Receta::class, 'Lleva', 'idIngrediente', 'idReceta');
    }**/

    public function EstaEn() 
    {
        return $this->belongsTo(Receta::class);
    }

}
