<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Valoracion;
use App\Models\Receta;
use App\Models\Recomendacion;
use Auth;

class PrincipalController extends Controller
{
    public function principal()
    {
        $sql = "SELECT r.id as recetaId, r.nombre, r.tipo, r.created_at, u.id as usuarioId, avg(puntuacion) as media, u.fotografia as fotoUsuario, r.fotografia as fotoReceta, u.name, r.descripcion FROM poga.recetas as r inner join poga.valoracions as v on r.id=v.id_receta inner join poga.users as u on r.id_usuario=u.id group by r.id order by media desc limit 3;";
        $tresMejores = DB::select($sql, [1]);
        $sql2 = "SELECT r.id  as recetaId, r.nombre, r.tipo, r.created_at, u.id as usuarioId, avg(puntuacion) as media, u.fotografia as fotoUsuario, r.fotografia as fotoReceta, u.name FROM poga.recetas as r inner join poga.valoracions as v on r.id=v.id_receta inner join poga.users as u on r.id_usuario=u.id group by v.id_receta order by avg(puntuacion) desc limit 3, 6;";
        $otrasSeis = DB::select($sql2, [1]);
        $sql3 = "SELECT r.id as recetaId, r.nombre, r.tipo, r.created_at, u.id as usuarioId, u.fotografia as fotoUsuario, r.fotografia as fotoReceta, u.name FROM poga.recetas as r inner join poga.users as u on r.id_usuario=u.id order by r.created_at desc limit 4;";
        $ultimas = DB::select($sql3, [1]);
        $sql4 = "SELECT r.id, r.fotografia FROM poga.recetas as r WHERE r.fotografia is not null AND r.fotografia != 'recetaDefault.jpg' ORDER BY RAND() LIMIT 10;";
        $fotos = DB::select($sql4, [1]);
        $usuario = Auth::user();
        $recomendaciones = null;
        if ($usuario != null) {
            $usuarioId = $usuario->id;
            $sql5 = "SELECT r.*, reco.top, u.*, r.updated_at AS recetaUpdate, r.id AS recetaId, u.id AS usuarioId, r.fotografia AS recetaFoto, u.fotografia AS usuarioFoto FROM poga.recetas AS r JOIN poga.recomendacions AS reco ON r.id = reco.id_receta JOIN poga.users AS u ON r.id_usuario = u.id WHERE reco.id_usuario = '$usuarioId'";
            $recomendaciones = DB::select($sql5, [1]);
        }
        
        return view('index')
            ->with('tresMejores', $tresMejores)
            ->with('otrasSeis', $otrasSeis)
            ->with('ultimas', $ultimas)
            ->with('usuario', $usuario)
            ->with('recomendaciones', $recomendaciones)
            ->with('fotos', $fotos);
    }

    /**public function recomendador()
    {
        echo "hola";
        $result = shell_exec("python3.7 " . public_path() . "/py/recomendador.py");
        dump($result);
        /**$command = escapeshellcmd('/public/py/holamon.py');
        $output = shell_exec($command);
        dump($command);
        echo $output;/
        die;
        echo "adios";
    }**/
}
