<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Receta;
use Auth;
use Illuminate\Support\Facades\Hash;
use \Datetime;

class UsuarioController extends Controller
{
    //

    public function verUsuario(Request $request)
    {
        try {
            $usuarioSesion = Auth::user();
            $usuario = User::findOrFail($request['id']);
            $recetas = DB::table('recetas')
                ->select('*')
                ->where('id_usuario', '=', $usuario->id)
                ->orderBy('created_at');
            return view ('showUser')
                ->with('usuario', $usuario)
                ->with('usuarioSesion', $usuarioSesion)
                ->with('recetas', $recetas->paginate(6));
        } catch (ModelNotFoundException $e) {
            return redirect('home');
        }
    }

    public function updateUsuarioRedirect()
    {
        if (Auth::id() == null)
            return redirect('logear');
        $usuario = Auth::user();
        return view ('updateUsuario')
            ->with('usuario', $usuario);

    }

    public function updateUsuario(Request $request)
    {
        $usuario = Auth::user();
        $credentials = ['email' => $usuario->email, 'password' => $request['password']];
        if (!Auth::attempt($credentials)) {
            return view ('updateUsuario')
            ->with('usuario', $usuario)
            ->with('contra', 'mal');
        }

        if (isset($request['foto'])) {
            $file = $request['foto'];
            $ruta = time().'.'.$file->extension();
            $file->move(public_path('img/usuario'), $ruta);
            $usuario->fotografia = $ruta;
        }
        if (isset($request['pais'])) {
            $pais = $request['pais'];
            $usuario->pais = $pais;
        }
        if (isset($request['ciudad'])) {
            $ciudad = $request['ciudad'];
            $usuario->ciudad = $ciudad;
        }
        if (isset($request['telefono'])) {
            $telefono = $request['telefono'];
            $usuario->telefono = $telefono;
        }

        $fechaString = strtotime($request['nacimiento']);
        $fecha = date('Y-m-d', $fechaString);
        $usuario->name = $request['name'];
        $usuario->apellido = $request['apellido'];
        $usuario->email = $request['email'];
        $usuario->genero = $request['genero'];
        $usuario->nacimiento = $fecha;
        $usuario->password = Hash::make($request['password']);
        $usuario->save();


    }

    public function registro()
    {
        if (Auth::id() != null)
            return view('home');
        return view('registro');
    }

    public function login()
    {
        if (Auth::id() != null)
            return view('home');
        return view('login');
    }
}
