<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Receta;
use App\Models\Ingrediente;
use App\Models\Valoracion;
use App\Models\User;
use App\Models\Paso;
use Auth;

class RecetaController extends Controller
{

    /**public function buscar(Request $request)
    {
        $usuario = Auth::user();
        $receta = DB::table('recetas');
        if (isset($request['nombre'])) {
            $nombre = $request['nombre'];
            $receta
                ->leftJoin('ingredientes', 'ingredientes.id_receta', '=', 'recetas.id')
                ->where('recetas.nombre', 'LIKE', '%'.$nombre.'%')
                ->orWhere('ingredientes.nombre', 'LIKE', '%'.$nombre.'%')
                ->orderBy('recetas.updated_at', 'desc');
        }
        return view('paginar', [
            'recetas' => $receta->paginate(6),
            'usuario' => $usuario
        ]);
    }**/

    public function filtrar(Request $request)
    {
        $usuario = Auth::user();
        $receta = DB::table('recetas')->select('recetas.*');
        if (isset($request['nombre']) && $request['nombre'] != null) {
            $nombre = $request['nombre'];
            $receta
                ->leftJoin('ingredientes', 'ingredientes.id_receta', '=', 'recetas.id')
                ->where('recetas.nombre', 'LIKE', '%'.$nombre.'%')
                ->orWhere('ingredientes.nombre', 'LIKE', '%'.$nombre.'%');
        }
        if (isset($request['origen'])) {
            $origen = $request['origen'];
            $receta->where('origen', '=', $origen);
        }
        if (isset($request['dificultad'])) {
            $dificultad = $request['dificultad'];
            $receta->where('dificultad', '=', $dificultad);
        }
        if (isset($request['tipo'])) {
            $tipo = $request['tipo'];
            $receta->where('tipo', '=', $tipo);
        }
        $receta->orderBy('recetas.updated_at', 'desc');
        return view('paginar', [
            'recetas' => $receta->paginate(6),
            'usuario' => $usuario
        ]);
    }

    /*public function check()
    {
        //$sql = "SELECT tipo, count(tipo) FROM poga.recetas GROUP BY tipo";
        $sql = "SELECT count(*) FROM poga.recetas";
        $recetas = DB::select($sql, [1]);
        //$recetas = Receta::select('SUM(tipo)')->groupBy('tipo')->get();
        dump($recetas);
        die;
    }*/

    /**public function probarJson()
    {

        $jsonString = array();
        $origen = array('Europea', 'Asiatica', 'Norteamericana', 'Sudamericana', 'Oceanica', 'Africana');
        $dificultad = array("Pinche", "Chef", "Masterchef");
        $tipo = array('Aperitivos y tapas', 'Arroces y cereales', 'Aves', 'Carnes', 'Ensaladas', 'Guisos y potajes', 'Legumbres', 'Mariscos', 'Pescados', 'Postres', 'Pasta', 'Salsa', 'Sopas y cremas', 'Verduras');
        for ($i = 0; $i < 100; $i++) {
            $tipoFinal = $tipo[random_int(0, 13)];

            switch ($tipoFinal) {
                case "Aperitivos y tapas":
                    $foto = "tapas.jpg";
                    break;
                case "Arroces y cereales":
                    $foto = "Arroz.jpg";
                    break;
                case "Aves":
                    $foto = "aves.jpg";
                    break;
                case "Carnes":
                    $foto = "carne.jpg";
                    break;
                case "Ensaladas":
                    $foto = "ensalada.jpg";
                    break;
                case "Guisos y potajes":
                    $foto = "potajes.jpg";
                    break;
                case "Legumbres":
                    $foto = "legumbres.jpg";
                    break;
                case "Mariscos":
                    $foto = "mariscos.jpg";
                    break;
                case "Pescados":
                    $foto = "pescado.jpg";
                    break;
                case "Postres":
                    $foto = "postre.jpg";
                    break;
                case "Pasta":
                    $foto = "pasta.jpg";
                    break;
                case "Salsa":
                    $foto = "salsa.jpg";
                    break;
                case "Sopas y cremas":
                    $foto = "crema.jpg";
                    break;
                case "Verduras":
                    $foto = "verdura.jpg";
                    break;
            }
            $receta = array(
                "nombre" => "Receta" . $i,
                "origen" => $origen[random_int(0, 5)],
                "dificultad" => $dificultad[random_int(0, 2)],
                "tipo" => $tipoFinal,
                "descripcion" => "Receta para algoritmo",
                "duracion" => random_int(1, 120),
                "personas" => random_int(1, 10),
                "fotografia" => $foto,
                "id_usuario" => random_int(1, 14)
            );
            array_push($jsonString, $receta);
        }
        $json = json_encode($jsonString, 0, 512);
        $path = storage_path() . "/json/prueba.json";
        file_put_contents($path, $json);
        dump($json);
        die;
    }**/

    /**public function crearValoracion()
    {
        $valoraciones = array();
        $sql = "SELECT * FROM poga.recetas";
        $recetas = DB::select($sql, [1]);
        $sql2 = "SELECT * FROM poga.users";
        $usuarios = DB::select($sql2, [1]);
        for ($j = 1; $j <= 15; $j++) {

            $aperitivos = random_int(1, 5);
            $arroces = random_int(1, 5);
            $aves = random_int(1, 5);
            $carnes = random_int(1, 5);
            $ensaladas = random_int(1, 5);
            $potajes = random_int(1, 5);
            $legumbres = random_int(1, 5);
            $mariscos = random_int(1, 5);
            $pescados = random_int(1, 5);
            $postres = random_int(1, 5);
            $pastas = random_int(1, 5);
            $salsas = random_int(1, 5);
            $sopas = random_int(1, 5);
            $verduras = random_int(1, 5);
            for ($i = 1; $i < count($recetas); $i++) {
                $vota = random_int(1, 2);
                if ($vota == 1) {
                    switch ($recetas[$i-1]->tipo) {
                        case "Aperitivos y tapas":
                            $puntuacion = $aperitivos;
                            break;
                        case "Arroces y cereales":
                            $puntuacion = $arroces;
                            break;
                        case "Aves":
                            $puntuacion = $aves;
                            break;
                        case "Carnes":
                            $puntuacion = $carnes;
                            break;
                        case "Ensaladas":
                            $puntuacion = $ensaladas;
                            break;
                        case "Guisos y potajes":
                            $puntuacion = $potajes;
                            break;
                        case "Legumbres":
                            $puntuacion = $legumbres;
                            break;
                        case "Mariscos":
                            $puntuacion = $mariscos;
                            break;
                        case "Pescados":
                            $puntuacion = $pescados;
                            break;
                        case "Postres":
                            $puntuacion = $postres;
                            break;
                        case "Pasta":
                            $puntuacion = $pastas;
                            break;
                        case "Salsa":
                            $puntuacion = $salsas;
                            break;
                        case "Sopas y cremas":
                            $puntuacion = $sopas;
                            break;
                        case "Verduras":
                            $puntuacion = $verduras;
                            break;
                    }
                    switch ($puntuacion) {
                        case 1:
                            $opinion = "Muy mala, no me ha gustado nada, los ingredientes no pegan, los tiempos estan mal...Un desastre!!";
                            break;
                        case 2:
                            $opinion = "Pff...He visto recetas mejores. No la recomiendo, creo que deberias esforzarte más.";
                            break;
                        case 3:
                            $opinion = "No está mal, peor creo que quedaría mejor con un poco menos de lo rojo, aun así la probaré.";
                            break;
                        case 4:
                            $opinion = "Que bueno!! me a gustado mucho, lástima la dificultad.";
                            break;
                        case 5:
                            $opinion = "Me ha encantado!!! Nunca había probado una receta tan rica!! Excelente!";
                            break;
                    }
                    $valoracion = array(
                        "id_receta" => $recetas[$i-1]->id,
                        "id_user" => $j,
                        "puntuacion" => $puntuacion,
                        "favorito" => 0,
                        "opinion" => $opinion
                    );
                    array_push($valoraciones, $valoracion);
                }
            }
        }

        $json = json_encode($valoraciones, 0, 512);
        $path = storage_path() . "/json/crearValoraciones.json";
        file_put_contents($path, $json);
        dump($json);
        die;
    }**/

    public function insertRecetaRedirect()
    {
        if (Auth::id() == null)
            return redirect('logear');
        return view ('insertarReceta');
    }

    public function updateRecetaRedirect(Request $request)
    {
        if (Auth::id() == null)
            return view('logear');
        try {
            $receta = Receta::findOrFail($request['id']);
            $ingredientes = Ingrediente::select('*')->where('id_receta', '=', $receta->id)->get();
            $pasos = Paso::select('*')->where('id_receta', '=', $receta->id)->get();
            $usuario = Auth::user();
        if ($usuario->id != $receta->id_usuario) {
            return redirect('home');
        }
        // return view ('updateReceta')
        return view ('updateReceipe')
            ->with('receta', $receta)
            ->with('usuario', $usuario)
            ->with('pasos', $pasos)
            ->with('ingredientes', $ingredientes);
        } catch (ModelNotFoundException $e) {
            return redirect('home');
        }
    }

    public function verReceta(Request $request)
    {
        try {
            $user = Auth::user();
            $recetaId = $request['id'];
            $sql2 = "SELECT r.*, u.*, r.updated_at AS recetaUpdate, r.id AS recetaId, u.id AS usuarioId, r.fotografia AS recetaFoto, u.fotografia AS usuarioFoto FROM poga.recetas AS r JOIN poga.users AS u ON r.id_usuario = u.id WHERE r.id = '$recetaId'";
            $receta = DB::select($sql2, [1]);
            if ($receta == null) {
                return redirect('home');
            }
            $ingredientes = Ingrediente::select('*')->where('id_receta', '=', $recetaId)->get();
            $sql = "SELECT u.name, u.fotografia, v.puntuacion, v.opinion, v.updated_at FROM poga.users AS u JOIN poga.valoracions AS v ON u.id = v.id_user WHERE v.id_receta = '$recetaId' AND v.opinion IS NOT NULL AND v.opinion != ''";
            $comentarios = DB::select($sql, [1]);
            // dump($comentarios);
            // die;
            $pasos = Paso::select('*')->where('id_receta', '=', $recetaId)->get();
            // $usuario = User::findOrFail($receta['id_usuario']);
            if(!Auth::guest()){
                $sql = "SELECT v.favorito FROM poga.valoracions as v inner join poga.users as u on v.id_user=u.id inner join poga.recetas as r on v.id_receta=r.id WHERE v.id_user = $user->id AND r.id = $recetaId;";
                $userId = $user->id;
                $valoracion = DB::select($sql, [1]);
            } else {
                $valoracion = null;
                $userId = null;
            }
            return view('receipe')
                // ->with('idUsuario', $userId)
                ->with('valoracion', $valoracion)
                ->with('receta', $receta[0])
                ->with('usuario', $user)
                ->with('pasos', $pasos)
                ->with('comentarios', $comentarios)
                ->with('ingredientes', $ingredientes);
        } catch (ModelNotFoundException $e) {
            return redirect('home');
        }
    }

    public function pruebaInsert(Request $request)
    {
        try {
            $usuario = Auth::user();
            return view('insertReceipe')
                ->with('usuario', $usuario);
        } catch (ModelNotFoundException $e) {
            return redirect('home');
        }
    }

    public function insertReceta(Request $request)
    {
        $request->validate([
            'nombre' => ['required', 'string', 'max:100'],
            'origen' => ['required', 'in:Europea,Asiatica,Norteamericana,Sudamericana,Oceanica,Africana'],
            'dificultad' => ['required', 'in:Pinche,Chef,Masterchef'],
            'tipo' => ['required', 'in:Aperitivos y tapas,Arroces y cereales,Aves,Carnes,Ensaladas,Guisos y potajes,Legumbres,Mariscos,Pescados,Postres,Pasta,Salsa,Sopas y cremas,Verduras'],
            'duracion' => ['required', 'numeric:min:1'],
            'personas' => ['required', 'numeric:min:1'],
        ]);
        if (Auth::id() == null)
            return redirect('logear');
        $ruta = null;
        if (isset($request['fotografia'])) {
            $request->validate([
                'fotografia' => 'required|file|mimes:jpeg,bmp,png,gif,webp',
            ]);
            $file = $request['fotografia'];
            $ruta = time().'.'.$file->extension();
            $file->move(public_path('img/recetas'), $ruta);
        } else {
            $ruta = "recetaDefault.jpg";
        }
        $usuario = Auth::user();
        $receta = new Receta();
        $receta->nombre = $request['nombre'];
        $receta->tipo = $request['tipo'];
        $receta->origen = $request['origen'];
        $receta->dificultad = $request['dificultad'];
        $receta->duracion = $request['duracion'];
        $receta->descripcion = $request['descripcion'];
        $receta->fotografia = $ruta;
        $receta->personas = $request['personas'];
        $receta->id_usuario = $usuario->id;
        $receta->save();
        $existe = true;
        $clave = 'paso1';
        $i = 2;
        $j = 1;
        while ($existe) {
            if (!isset($request[$clave])) {
                $existe = false;
            }
            else {
                if ($request[$clave] != "") {
                    $paso = new Paso();
                    $paso->numero = $j;
                    $paso->descripcion = $request[$clave];
                    $paso->id_receta = $receta->id;
                    $paso->save();
                    $j++;
                }
                $clave = substr_replace($clave, strval($i), -1);
                $i++;
            }
        }
        $existe = true;
        $clave = 'ingrediente1';
        $clave2 = 'cantidad1';
        $i = 2;
        $j = 1;
        while ($existe) {
            if (!isset($request[$clave])) {
                $existe = false;
            }
            else {
                if ($request[$clave] != "") {
                    $ingrediente = new Ingrediente();
                    $ingrediente->nombre = $request[$clave];
                    $ingrediente->cantidad = $request[$clave2];
                    $ingrediente->id_receta = $receta->id;
                    $ingrediente->save();
                    $j++;
                }
                $clave = substr_replace($clave, strval($i), -1);
                $clave2 = substr_replace($clave2, strval($i), -1);
                $i++;
            }
        }
        return redirect("/verReceta/".$receta['id']);
    }

    public function updateReceta(Request $request)
    {
        try {
            $receta = Receta::findOrFail($request['id']);
            $ingredientes = Ingrediente::select('*')->where('id_receta', '=', $receta->id)->get();
            $pasos = Paso::select('*')->where('id_receta', '=', $receta->id)->get();
            if (isset($request['fotografia'])) {
                $request->validate([
                    'fotografia' => 'required|file|mimes:jpeg,bmp,png,gif,webp',
                ]);
                $file = $request['fotografia'];
                $ruta = time().'.'.$file->extension();
                $file->move(public_path('img/recetas'), $ruta);
                if ($receta->fotografia != "recetaDefault.jpg") {
                    $rutaTotal = public_path('img/recetas/');
                    $rutaTotal .= $receta->fotografia;
                    unlink($rutaTotal);
                }
                $receta->fotografia = $ruta;
            }
            $existe = true;
            $clave = 'paso1';
            $i = 2;
            $j = 1;
            foreach ($pasos as $paso) {
                $paso->delete();
            }
            while ($existe) {
                if (!isset($request[$clave])) {
                    $existe = false;
                }
                else {
                    if ($request[$clave] != "") {
                        $paso = new Paso();
                        $paso->numero = $j;
                        $paso->descripcion = $request[$clave];
                        $paso->id_receta = $receta->id;
                        $paso->save();
                        $j++;
                    }
                    $clave = substr_replace($clave, strval($i), -1);
                    $i++;
                }
            }

            $existe = true;
            $clave = 'ingrediente1';
            $clave2 = 'cantidad1';
            $i = 2;
            $j = 1;
            foreach ($ingredientes as $ingrediente) {
                $ingrediente->delete();
            }
            while ($existe) {
                if (!isset($request[$clave])) {
                    $existe = false;
                }
                else {
                    if ($request[$clave] != "") {
                        $ingrediente = new Ingrediente();
                        $ingrediente->nombre = $request[$clave];
                        $ingrediente->cantidad = $request[$clave2];
                        $ingrediente->id_receta = $receta->id;
                        $ingrediente->save();
                        $j++;
                    }
                    $clave = substr_replace($clave, strval($i), -1);
                    $clave2 = substr_replace($clave2, strval($i), -1);
                    $i++;
                }
            }
            $receta->nombre = $request['nombre'];
            $receta->tipo = $request['tipo'];
            $receta->origen = $request['origen'];
            $receta->dificultad = $request['dificultad'];
            $receta->duracion = $request['duracion'];
            $receta->personas = $request['personas'];
            $receta->descripcion = $request['descripcion'];
            $receta->save();
            return redirect("/verReceta/".$receta['id']);
        } catch (ModelNotFoundException $e) {
            return redirect('home');
        }
    }

    public function deleteReceta(Request $request)
    {
        try {
            $usuario = Auth::user();
            $receta = Receta::findOrFail($request['receta']);
            if ($usuario->id != $receta->id_usuario) {
                return redirect('home');
            }
            $valoracion = Valoracion::select('*')->where('id_receta', '=', $request['receta'])->get();
            foreach ($valoracion as $val) {
                $val->delete();
            }
            $receta->delete();
            return redirect('home');
        } catch (ModelNotFoundException $e) {
            return redirect('home');
        }
    }
}
