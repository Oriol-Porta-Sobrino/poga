<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use \Datetime;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'apellido' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'foto' => ['file', 'mimes:jpeg,bmp,png,gif,webp'],
            'genero'=> ['in:Hombre,Mujer'],
            'nacimiento' => ['required', 'before:tomorrow'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $ruta = null;
        $ciudad = null;
        $pais = null;
        $telefono = null;
        if (isset($data['foto'])) {
            $file = $data['foto'];
            $ruta = time().'.'.$file->extension();
            $file->move(public_path('img/usuarios'), $ruta);
        } else {
            $ruta = "usuarioDefault.jpeg";
        }
        if (isset($data['pais'])) {
            $pais = $data['pais'];
        }
        if (isset($data['ciudad'])) {
            $ciudad = $data['ciudad'];
        }
        if (isset($data['telefono'])) {
            $telefono = $data['telefono'];
        }
        $fechaString = strtotime($data['nacimiento']);
        $fecha = date('d-m-Y', $fechaString);
        return User::create([
            'name' => $data['name'],
            'apellido' => $data['apellido'],
            'email' => $data['email'],
            'genero' => $data['genero'],        
            'nacimiento' => $data['nacimiento'],
            'pais' => $pais,
            'ciudad' => $ciudad,
            'telefono' => $telefono,     
            'fotografia' => $ruta,
            'password' => Hash::make($data['password']),
        ]);
    }
}
