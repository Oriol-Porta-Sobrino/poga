<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class TemporalController extends Controller
{
    public function insertReceta(array $data) 
    {
        if (Auth::id() == null) 
            return view('auth/login');
        $usuario = Auth::user();
        $receta = new Receta();
        $receta->nombre = $data['nombre'];
        $receta->tipo = $data['tipo'];
        $receta->origen = $data['origen'];
        $receta->dificultad = $data['dificultad'];
        $receta->duracion = $data['duracion'];
        $receta->fotografia = $data['fotografia'];
        $receta->personas = $data['personas'];
        $receta->id_usuario = $usuario->id;
        $receta->save();
    }

    public function updateReceta(array $data)
    {
        $receta = $data['receta'];
        $receta->nombre = $data['nombre'];
        $receta->tipo = $data['tipo'];
        $receta->origen = $data['origen'];
        $receta->dificultad = $data['dificultad'];
        $receta->duracion = $data['duracion'];
        $receta->fotografia = $data['fotografia'];
        $receta->personas = $data['personas'];
        $receta->save();
    }

    public function deleteReceta(array $data)
    {
        $receta = $data['receta'];
        $receta->delete();
    }

}
