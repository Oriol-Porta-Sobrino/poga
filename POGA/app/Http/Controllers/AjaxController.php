<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function index(Request $request) {
        $mensaje = $request['boton'];
        $msg = "This is a simple message.";
        return response()->json(array('msg'=> $msg . $mensaje), 200);
     }
}
