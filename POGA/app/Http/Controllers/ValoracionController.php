<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Valoracion;
use Auth;

class ValoracionController extends Controller
{
    public function like(Request $request) 
    {
        if (Auth::id() == null) 
            return view('logear');
        $usuario = Auth::user();
        $recetaId = $request['receta'];
        $valoracion = Valoracion::select('*')->where('id_receta', '=', $recetaId)->where('id_user', '=', $usuario->id)->first();
    
        if ($valoracion === null) {
            $valoracion = new Valoracion();
            $valoracion->id_receta = $recetaId;
            $valoracion->id_user = $usuario->id;
        } 
        
        if ($valoracion->favorito == null || $valoracion->favorito == 0) {
            $valoracion->favorito = 1;
        }
        else {
            $valoracion->favorito = 0;
        }
        $valoracion->save();

        return response()->json(array('fav'=> $valoracion->favorito), 200);

    }

    public function puntuar(Request $request)
    {
        if (Auth::id() == null) 
            return view('logear');
        $usuario = Auth::user();
        $recetaId = $request['receta'];
        $newPuntuacion = $request['puntos'];
        $valoracion = Valoracion::select('*')->where('id_receta', '=', $recetaId)->where('id_user', '=', $usuario->id)->first();
        if ($valoracion === null) {
            $valoracion = new Valoracion();
            $valoracion->id_receta = $recetaId;
            $valoracion->id_user = $usuario->id;
        }
        $valoracion->puntuacion = $newPuntuacion;
        $valoracion->save();
        $result = shell_exec("python3.7 " . public_path() . "/py/recomendador.py");
        return response()->json(array('puntos'=> $valoracion->puntuacion), 200);
    }

    public function comentar(Request $request)
    {
        if (Auth::id() == null) 
            return view('logear');
        $usuario = Auth::user();
        $recetaId = $request['id'];
        $comentario = $request['comentario'];
        $valoracion = Valoracion::select('*')->where('id_receta', '=', $recetaId)->where('id_user', '=', $usuario->id)->first();
        if ($valoracion === null) {
            $valoracion = new Valoracion();
            $valoracion->id_receta = $recetaId;
            $valoracion->id_user = $usuario->id;
        }
        $valoracion->opinion = $comentario;
        $valoracion->save();
        return redirect("/verReceta/".$recetaId);
    }   
}
