<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
    </script>
</head>
<body>
    
    <form method="POST" enctype="multipart/form-data" action="{{ route('anadirReceta') }}" id="form">
        @csrf
        <label>Nombre</label>
        <input type="text" name="nombre">
        @error('nombre')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
        <label>Origen</label>
        <select name="origen">   
            <option value="" selected disabled hidden>Escoge origen</option>   
            <option value="Europea">Europea</option>
            <option value="Asiatica">Asiatica</option>
            <option value="Norteamericana">Norteamericana</option>
            <option value="Sudamericana">Sudamericana</option>
            <option value="Oceanica">Oceanica</option>
            <option value="Africana">Africana</option>
        </select>
        <label>Dificultad</label>
        <select name="dificultad">
            <option value="" selected disabled hidden>Escoge dificultad</option>  
            <option value="Pinche">Pinche</option>
            <option value="Chef">Chef</option>
            <option value="Masterchef">Masterchef</option>
        </select>
        <label>Tipo</label>
        <select name="tipo">
            <option value="" selected disabled hidden>Escoge tipo</option>  
            <option value="Aperitivos y tapas" >Aperitivos y tapas</option>
            <option value="Arroces y cereales" >Arroces y cereales</option>
            <option value="Aves" >Aves</option>
            <option value="Carnes" >Carnes</option>
            <option value="Ensaladas" >Ensaladas</option>
            <option value="Guisos y potajes" >Guisos y potajes</option>
            <option value="Legumbres" >Legumbres</option>
            <option value="Mariscos" >Mariscos</option>
            <option value="Pescados" >Pescados</option>
            <option value="Postres" >Postres</option>
            <option value="Pasta" >Pasta</option>
            <option value="Salsa" >Salsa</option>
            <option value="Sopas y cremas" >Sopas y cremas</option>
            <option value="Verduras" >Verduras</option>
        </select>
        <label>Duracion</label>
        <input type="number" name="duracion">
        <label>Personas</label>
        <input type="number" name="personas">
        <label>Descripcion</label>
        <input type="text" name="descripcion">
        <label>Fotografia</label>
        <input type="file" name="fotografia">
        <input type="submit" name="subida">
        <div id="pasos"></div>
        <div id="ingredientes"></div>
    </form>
    
    <button onclick="addPaso()">Añadir paso</button>
    <button onclick="addIngrediente()">Añadir ingrediente</button>

    <script src="{{ asset('js/insertarReceta/paso.js')}}"></script>
    <script src="{{ asset('js/insertarReceta/ingrediente.js')}}"></script>
</body>
</html>