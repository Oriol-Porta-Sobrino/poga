<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Title -->
    <title>POGA Receptes - IA Recommender - updateRecipes</title>

    <!-- Favicon -->
    <link rel="icon" href="{{ URL::asset('img/index_recipes/core-img/favicon.ico') }}">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="{{ URL::asset('css/index_recipes/styleNewRecipes.css') }}">
    <!-- <link rel="stylesheet" href="{{ URL::asset('css/index_recipes/bootstrap.min.css') }}"> -->

</head>

<body onload="carga({{$pasos}}, {{$ingredientes}})">
<!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="preloader-content">
            <h3>Cooking in progress..</h3>
            <div id="cooking">
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div id="area">
                    <div id="sides">
                        <div id="pan"></div>
                        <div id="handle"></div>
                    </div>
                    <div id="pancake">
                        <div id="pastry"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area">

        <!-- Top Header Area -->
        <div class="top-header-area bg-img bg-overlay">
            <div class="container h-100">
                <div class="row h-100 align-items-center justify-content-between">
                    <div class="col-12 col-sm-6">
                        <!-- Top Social Info -->
                        <div class="top-social-info">
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Pinterest"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-5 col-xl-4">
                        <!-- Top Search Area -->
                        <div class="top-search-area">
                            <form action="#" method="post">
                                <input type="search" name="top-search" id="topSearch" placeholder="Search">
                                <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Logo Area -->
        <div class="logo-area">
            <a href="{{ url('/..') }}"><img src="{{URL::asset('img/index_recipes/core-img/poga3.png')}}" alt=""></a>
        </div>

        <!-- Navbar Area -->
        <div class="bueno-main-menu" id="sticker">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Menu -->
                    <nav class="classy-navbar justify-content-between" id="buenoNav">

                        <!-- Toggler -->
                        <div id="toggler"><img src="{{URL::asset('img/index_recipes/core-img/toggler.png')}}" alt=""></div>

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">

                            <!-- Close Button -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                    <li><a href="{{ url('/..') }}">Home</a></li>
                                    <li><a href="#exampleModal" data-toggle="modal" data-target="#exampleModal">About Us</a></li>
                                    <li><a href="https://agora.xtec.cat/ies-sabadell/">Contact</a></li>
                                </ul>

                                <!-- Login/Register -->
                                @if( Auth::guest() )
                                <div class="login-area">
                                    <a href="{{ url('/logear') }}">Login</a> / <a href="{{ url('/registrarse') }}">Registrar</a>
                                </div>
                                @else
                                <ul class="ullogin">
                                    <li><a href="#"><i class="fa fa fa-user"></i> &nbsp;{{$usuario->name}}</a>
                                        <ul class="dropdown">
                                            <li><a href="{{ url('/verUsuario/' . $usuario->id) }}"><i class="fa fa-user"></i> &nbsp;Area Personal</a></li>
                                            <li><a href="{{ url('/insertReceta') }}"><i class="fa fa-plus"></i> &nbsp;Nueva Receta</a></li>
                                            <li class="salir"><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> &nbsp;Logout</a></li>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                                        </ul>
                                    </li>
                                </ul>
                                @endif
                            </div>
                            <!-- Nav End -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ##### Header Area End ##### -->
    <!-- ##### Treading Post Area Start ##### -->
    <div class="treading-post-area" id="treadingPost">
        <div class="close-icon">
            <i class="fa fa-times"></i>
        </div>

        <h4>Treading Post</h4>

        <!-- Single Blog Post -->
        <div class="single-blog-post style-1 d-flex flex-wrap mb-30">
            <!-- Blog Thumbnail -->
            <div class="blog-thumbnail">
                <img src="{{URL::asset('img/index_recipes/bg-img/9.jpg')}}" alt="">
            </div>
            <!-- Blog Content -->
            <div class="blog-content">
                <a href="#" class="post-tag">The Best</a>
                <a href="#" class="post-title">Friend eggs with ham</a>
                <div class="post-meta">
                    <a href="#" class="post-date">July 11, 2018</a>
                    <a href="#" class="post-author">By Julia Stiles</a>
                </div>
            </div>
        </div>

        <!-- Single Blog Post -->
        <div class="single-blog-post style-1 d-flex flex-wrap mb-30">
            <!-- Blog Thumbnail -->
            <div class="blog-thumbnail">
                <img src="{{URL::asset('img/index_recipes/bg-img/10.jpg')}}" alt="">
            </div>
            <!-- Blog Content -->
            <div class="blog-content">
                <a href="#" class="post-tag">The Best</a>
                <a href="#" class="post-title">Mushrooms with pork chop</a>
                <div class="post-meta">
                    <a href="#" class="post-date">July 11, 2018</a>
                    <a href="#" class="post-author">By Julia Stiles</a>
                </div>
            </div>
        </div>

        <!-- Single Blog Post -->
        <div class="single-blog-post style-1 d-flex flex-wrap mb-30">
            <!-- Blog Thumbnail -->
            <div class="blog-thumbnail">
                <img src="{{URL::asset('img/index_recipes/bg-img/11.jpg')}}" alt="">
            </div>
            <!-- Blog Content -->
            <div class="blog-content">
                <a href="#" class="post-tag">The Best</a>
                <a href="#" class="post-title">Birthday cake with chocolate</a>
                <div class="post-meta">
                    <a href="#" class="post-date">July 11, 2018</a>
                    <a href="#" class="post-author">By Julia Stiles</a>
                </div>
            </div>
        </div>

        <!-- Single Blog Post -->
        <div class="single-blog-post style-1 d-flex flex-wrap mb-30">
            <!-- Blog Thumbnail -->
            <div class="blog-thumbnail">
                <img src="{{URL::asset('img/index_recipes/bg-img/9.jpg')}}" alt="">
            </div>
            <!-- Blog Content -->
            <div class="blog-content">
                <a href="#" class="post-tag">The Best</a>
                <a href="#" class="post-title">Friend eggs with ham</a>
                <div class="post-meta">
                    <a href="#" class="post-date">July 11, 2018</a>
                    <a href="#" class="post-author">By Julia Stiles</a>
                </div>
            </div>
        </div>

        <!-- Single Blog Post -->
        <div class="single-blog-post style-1 d-flex flex-wrap mb-30">
            <!-- Blog Thumbnail -->
            <div class="blog-thumbnail">
                <img src="{{URL::asset('img/index_recipes/bg-img/10.jpg')}}" alt="">
            </div>
            <!-- Blog Content -->
            <div class="blog-content">
                <a href="#" class="post-tag">The Best</a>
                <a href="#" class="post-title">Mushrooms with pork chop</a>
                <div class="post-meta">
                    <a href="#" class="post-date">July 11, 2018</a>
                    <a href="#" class="post-author">By Julia Stiles</a>
                </div>
            </div>
        </div>

        <!-- Single Blog Post -->
        <div class="single-blog-post style-1 d-flex flex-wrap mb-30">
            <!-- Blog Thumbnail -->
            <div class="blog-thumbnail">
                <img src="{{URL::asset('img/index_recipes/bg-img/11.jpg')}}" alt="">
            </div>
            <!-- Blog Content -->
            <div class="blog-content">
                <a href="#" class="post-tag">The Best</a>
                <a href="#" class="post-title">Birthday cake with chocolate</a>
                <div class="post-meta">
                    <a href="#" class="post-date">July 11, 2018</a>
                    <a href="#" class="post-author">By Julia Stiles</a>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Treading Post Area End ##### -->

    <!-- ##### Post Details Area Start ##### -->
    <section class="post-news-area section-padding-100-0 mb-70">
        <div class="container">
            <form method="POST" enctype="multipart/form-data" action="{{ route('updateReceta') }}" id="form">
                @csrf
                <input type="hidden" name="id" value="{{$receta['id']}}">
                <div class="row justify-content-center">
                    <!-- Post Details Content Area -->
                    <div class="col-12 col-lg-4 col-xl-5">
                        <div class="post-details-content mb-100">
                            <!-- DropZone fileUpload -->
                            <div class="file-upload">
                                <div class="image-upload-wrap">
                                    <input class="file-upload-input" type='file' name="fotografia"  onchange="readURL(this);" accept="image/*" />
                                    <div class="drag-text">
                                        <h3>Arrastre y suelte una imagen o pulse para seleccionar</h3>
                                    </div>
                                </div>
                                <div class="file-upload-content">
                                    <img class="file-upload-image" src="#" alt="your image" />
                                    <div class="image-title-wrap">
                                        <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <!-- DropZone fileUpload End-->
                            <div class="ingredients">
                                <h5>Descripción</h5>
                                <textarea class="form-control" name="descripcion" >{{$receta['descripcion']}}</textarea>
                            </div>
                            <div class="blog-content">
                                <br>
                                <div id="pasos"></div>
                                <h4 class="post-title"><button type="button" class="btn btn-success" onclick="addPaso()">
                                <img src="{{URL::asset('img/index_recipes/core-img/plus.png')}}"></button> Pasos </h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4 col-xl-4">
                        <!-- Info -->
                        <div class="recipe-info">
                            <ul class="info-data">
                                <li><span>
                                    <label>Título *</label>
                                    <input class="form-control" type="text" name="nombre" value="{{$receta['nombre']}}">
                                    @error('nombre')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </span></li>
                                <li><span><label>Duración *</label><input class="form-control" type="number" name="duracion" min="1" value="{{$receta['duracion']}}"></span></li>
                                <li><span><label>Comensales *</label><input class="form-control" type="number" name="personas" min="1" value="{{$receta['personas']}}"></span></li>
                                <li><span><label>Dificultad *</label>
                                <select class="form-control" name="dificultad">
                                    <option @if ($receta['dificultad'] == 'Pinche') selected @endif value="Pinche">Pinche</option>
                                    <option @if ($receta['dificultad'] == 'Chef') selected @endif value="Chef">Chef</option>
                                    <option @if ($receta['dificultad'] == 'Masterchef') selected @endif value="Masterchef">Masterchef</option>
                                </select></span></li>
                                <li><span><label>Origen *</label>
                                <select class="form-control" name="origen">
                                    <option value="" selected disabled hidden>Escoge origen</option>
                                    <option @if ($receta['origen'] == 'Europea') selected @endif value="Europea">Europea</option>
                                    <option @if ($receta['origen'] == 'Asiatica') selected @endif value="Asiatica">Asiatica</option>
                                    <option @if ($receta['origen'] == 'Norteamericana') selected @endif value="Norteamericana">Norteamericana</option>
                                    <option @if ($receta['origen'] == 'Sudamericana') selected @endif value="Sudamericana">Sudamericana</option>
                                    <option @if ($receta['origen'] == 'Oceanica') selected @endif value="Oceanica">Oceanica</option>
                                    <option @if ($receta['origen'] == 'Africana') selected @endif value="Africana">Africana</option>
                                </select></span></li>
                                <li><span><label>Tipo de receta *</label>
                                <select class="form-control" name="tipo">
                                    <option @if ($receta['tipo'] == 'Aperitivos y tapas') selected @endif value="Aperitivos y tapas" >Aperitivos y tapas</option>
                                    <option @if ($receta['tipo'] == 'Arroces y cereales') selected @endif value="Arroces y cereales" >Arroces y cereales</option>
                                    <option @if ($receta['tipo'] == 'Aves') selected @endif value="Aves" >Aves</option>
                                    <option @if ($receta['tipo'] == 'Carnes') selected @endif value="Carnes" >Carnes</option>
                                    <option @if ($receta['tipo'] == 'Ensaladas') selected @endif value="Ensaladas" >Ensaladas</option>
                                    <option @if ($receta['tipo'] == 'Guisos y potajes') selected @endif value="Guisos y potajes" >Guisos y potajes</option>
                                    <option @if ($receta['tipo'] == 'Legumbres') selected @endif value="Legumbres" >Legumbres</option>
                                    <option @if ($receta['tipo'] == 'Mariscos') selected @endif value="Mariscos" >Mariscos</option>
                                    <option @if ($receta['tipo'] == 'Pescados') selected @endif value="Pescados" >Pescados</option>
                                    <option @if ($receta['tipo'] == 'Postres') selected @endif value="Postres" >Postres</option>
                                    <option @if ($receta['tipo'] == 'Pasta') selected @endif value="Pasta" >Pasta</option>
                                    <option @if ($receta['tipo'] == 'Salsa') selected @endif value="Salsa" >Salsa</option>
                                    <option @if ($receta['tipo'] == 'Sopas y cremas') selected @endif value="Sopas y cremas" >Sopas y cremas</option>
                                    <option @if ($receta['tipo'] == 'Verduras') selected @endif value="Verduras" >Verduras</option>
                                </select></span>
                                </li>
                            </ul>
                        </div>

                        <!-- Ingredients -->
                        <div class="blog-content">
                            <br>
                            <div id="ingredientes"></div>
                            <h4 class="post-title"><button type="button" class="btn btn-success" onclick="addIngrediente()">
                            <img src="{{URL::asset('img/index_recipes/core-img/plus.png')}}"></button> Ingredientes </h4>
                        </div>
                    </div>
                    <!-- Sidebar Widget -->
                    <div class="col-12 col-sm-9 col-md-6 col-lg-4 col-xl-3">
                        <div class="sidebar-area">
                            <!-- Single Widget Area -->
                            <div class="single-widget-area author-widget mb-30">
                                <div class="background-pattern bg-img">
                                    <div class="author-thumbnail">
                                        <img src="{{ asset('img/usuarios/' . $usuario['fotografia']) }}" alt="">
                                    </div>
                                    <p>My name is <span>{{$usuario['name']}}</span>, I’m a passionate cook with a love Pol</p>
                                </div>
                                <div class="social-info">
                                    <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                </div><br>
                                <button type="submit" name="subida" class="btn btn-outline-success btn-lg btn-block">Subir receta</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
    <!-- ##### Post Details Area End ##### -->
    <!-- ##### Footer Area Start ##### -->
    <footer class="footer-area">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-5">
                   <!-- Copywrite Text -->
                   <p class="copywrite-text"><a href="#">
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | by <a href="https://agora.xtec.cat/ies-sabadell/" target="_blank">POGA</a>
                    </p>
                </div>
                <div class="col-12 col-sm-7">
                    <!-- Footer Nav -->
                    <div class="footer-nav">
                        <ul>
                            <li class="active"><a href="{{ url('/..') }}">Home</a></li>
                            <li><a href="#exampleModal" data-toggle="modal" data-target="#exampleModal">About Us</a></li>
                            <li><a href="https://agora.xtec.cat/ies-sabadell/">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">POGA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <div class="modal-body">
            <p>Somos un grupo de alumnos formado por<br><b>Oriol Porta, Pol Garcia e Isaac García</b>. Juntos formamos POGA, sociendad formada para realizar el proyecto final de CFGS Desenvolupaent d'aplicacions Multiplataforma.</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
    </div>
    <!-- ##### Footer Area End ##### -->
    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script class="jsbin" src="{{URL::asset('https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js')}}"></script>
    <script src="{{ asset('js/index_recipes/jquery/jquery-2.2.4.min.js')}}"></script>
    <!-- Popper js -->
    <script src="{{ asset('js/index_recipes/bootstrap/popper.min.js')}}"></script>
    <!-- Bootstrap js -->
    <script src="{{ asset('js/index_recipes/bootstrap/bootstrap.min.js')}}"></script>
    <!-- All Plugins js -->
    <script src="{{ asset('js/index_recipes/plugins/plugins.js')}}"></script>
    <script  src="{{ asset('js/insertarReceta/scriptDropzone.js')}}"></script>
    <script src="{{ asset('js/updateReceta/addInputs.js')}}"></script>
    <!-- Active js -->
    <script src="{{ asset('js/index_recipes/active.js')}}"></script>
</body>
</html>