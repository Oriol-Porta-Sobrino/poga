<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>POGA Receptes &#9679; IA Recommender</title>

    <!-- Favicon -->
    <link rel="icon" href="{{ URL::asset('img/core-img/favicon.ico') }}">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="{{ URL::asset('css/index_recipes/style.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/index_recipes/styleIARecommender.css') }}">

</head>

<body>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="preloader-content">
            <h3>Cooking in progress..</h3>
            <div id="cooking">
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div id="area">
                    <div id="sides">
                        <div id="pan"></div>
                        <div id="handle"></div>
                    </div>
                    <div id="pancake">
                        <div id="pastry"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area">
        <!-- Top Header Area -->
        <div class="top-header-area bg-img bg-overlay">
            <div class="container h-100">
                <div class="row h-100 align-items-center justify-content-between">
                    <div class="col-12 col-sm-6">
                        <!-- Top Social Info -->
                        <div class="top-social-info">
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Pinterest"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-5 col-xl-4">
                        <!-- Top Search Area -->
                        <div class="top-search-area">
                            <form method="GET" enctype="multipart/form-data" action="{{ route('buscar') }}">
                                @csrf
                                <input type="search" name="nombre" id="topSearch" placeholder="Search">
                                <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Logo Area -->
        <div class="logo-area">
            <a href="{{ url('/..') }}"><img src="{{URL::asset('img/index_recipes/core-img/poga3.png')}}" alt=""></a>
        </div>

        <!-- Navbar Area -->
        <div class="bueno-main-menu" id="sticker">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Menu -->
                    <nav class="classy-navbar justify-content-between" id="buenoNav">

                        <!-- Toggler -->
                        <div id="toggler"><img src="{{URL::asset('img/index_recipes/core-img/toggler.png')}}" alt=""></div>

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">

                            <!-- Close Button -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                    <li><a href="{{ url('/..') }}">Home</a></li>
                                    <li><a href="#exampleModal" data-toggle="modal" data-target="#exampleModal">About Us</a></li>
                                    <li><a href="https://agora.xtec.cat/ies-sabadell/">Contact</a></li>
                                </ul>

                                <!-- Login/Register -->
                                @if( Auth::guest() )
                                <div class="login-area">
                                    <a href="{{ url('/logear') }}">Login</a> / <a href="{{ url('/registrarse') }}">Registrar</a>
                                </div>
                                @else
                                <ul class="ullogin">
                                    <li><a href="#"><i class="fa fa fa-user"></i> &nbsp;{{$usuario->name}}</a>
                                        <ul class="dropdown">
                                            <li><a href="{{ url('/verUsuario/' . $usuario->id) }}"><i class="fa fa-user"></i> &nbsp;Area Personal</a></li>
                                            <li><a href="{{ url('/insertReceta') }}"><i class="fa fa-plus"></i> &nbsp;Nueva Receta</a></li>
                                            <li class="salir"><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> &nbsp;Logout</a></li>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                                        </ul>
                                    </li>
                                </ul>
                                @endif
                            </div>
                            <!-- Nav End -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ##### Treading Post Area Start ##### -->
    <div class="treading-post-area" id="treadingPost">
        <div class="close-icon">
            <i class="fa fa-times"></i>
        </div>

        <h4>Treading Post</h4>

        <!-- Single Blog Post -->
        <div class="single-blog-post style-1 d-flex flex-wrap mb-30">
            <!-- Blog Thumbnail -->
            <div class="blog-thumbnail">
                <img src="{{ URL::asset('img/index_recipes/bg-img/9.jpg') }}" alt="">
            </div>
            <!-- Blog Content -->
            <div class="blog-content">
                <a href="#" class="post-tag">The Best</a>
                <a href="#" class="post-title">Friend eggs with ham</a>
                <div class="post-meta">
                    <a href="#" class="post-date">July 11, 2018</a>
                    <a href="#" class="post-author">By Julia Stiles</a>
                </div>
            </div>
        </div>

        <!-- Single Blog Post -->
        <div class="single-blog-post style-1 d-flex flex-wrap mb-30">
            <!-- Blog Thumbnail -->
            <div class="blog-thumbnail">
            <img src="{{ URL::asset('img/index_recipes/bg-img/10.jpg') }}" alt="">
            </div>
            <!-- Blog Content -->
            <div class="blog-content">
                <a href="#" class="post-tag">The Best</a>
                <a href="#" class="post-title">Mushrooms with pork chop</a>
                <div class="post-meta">
                    <a href="#" class="post-date">July 11, 2018</a>
                    <a href="#" class="post-author">By Julia Stiles</a>
                </div>
            </div>
        </div>

        <!-- Single Blog Post -->
        <div class="single-blog-post style-1 d-flex flex-wrap mb-30">
            <!-- Blog Thumbnail -->
            <div class="blog-thumbnail">
                <img src="img/index_recipes/bg-img/11.jpg" alt="">
            </div>
            <!-- Blog Content -->
            <div class="blog-content">
                <a href="#" class="post-tag">The Best</a>
                <a href="#" class="post-title">Birthday cake with chocolate</a>
                <div class="post-meta">
                    <a href="#" class="post-date">July 11, 2018</a>
                    <a href="#" class="post-author">By Julia Stiles</a>
                </div>
            </div>
        </div>

        <!-- Single Blog Post -->
        <div class="single-blog-post style-1 d-flex flex-wrap mb-30">
            <!-- Blog Thumbnail -->
            <div class="blog-thumbnail">
                <img src="img/index_recipes/bg-img/9.jpg" alt="">
            </div>
            <!-- Blog Content -->
            <div class="blog-content">
                <a href="#" class="post-tag">The Best</a>
                <a href="#" class="post-title">Friend eggs with ham</a>
                <div class="post-meta">
                    <a href="#" class="post-date">July 11, 2018</a>
                    <a href="#" class="post-author">By Julia Stiles</a>
                </div>
            </div>
        </div>

        <!-- Single Blog Post -->
        <div class="single-blog-post style-1 d-flex flex-wrap mb-30">
            <!-- Blog Thumbnail -->
            <div class="blog-thumbnail">
                <img src="img/index_recipes/bg-img/10.jpg" alt="">
            </div>
            <!-- Blog Content -->
            <div class="blog-content">
                <a href="#" class="post-tag">The Best</a>
                <a href="#" class="post-title">Mushrooms with pork chop</a>
                <div class="post-meta">
                    <a href="#" class="post-date">July 11, 2018</a>
                    <a href="#" class="post-author">By Julia Stiles</a>
                </div>
            </div>
        </div>

        <!-- Single Blog Post -->
        <div class="single-blog-post style-1 d-flex flex-wrap mb-30">
            <!-- Blog Thumbnail -->
            <div class="blog-thumbnail">
                <img src="img/index_recipes/bg-img/11.jpg" alt="">
            </div>
            <!-- Blog Content -->
            <div class="blog-content">
                <a href="#" class="post-tag">The Best</a>
                <a href="#" class="post-title">Birthday cake with chocolate</a>
                <div class="post-meta">
                    <a href="#" class="post-date">July 11, 2018</a>
                    <a href="#" class="post-author">By Julia Stiles</a>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Search Area Start ##### -->
    <div class="bueno-search-area section-padding-100-0 pb-70">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <form method="GET" enctype="multipart/form-data" action="{{ route('filtrar') }}" id="form">
                        @csrf
                        <div class="row">
                            <div class="col-12 col-sm-3">
                                <div class="form-group mb-30">
                                    <select class="form-control" name="origen">
                                        <option value="" disabled selected hidden>Origen</option>
                                        <option value="Europea">Europea</option>
                                        <option value="Asiatica">Asiatica</option>
                                        <option value="Norte-Americana">Norte-Americana</option>
                                        <option value="Sud-Americana">Sud-Americana</option>
                                        <option value="Oceanica">Oceanica</option>
                                        <option value="Africana">Africana</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-12 col-sm-3">
                                <div class="form-group mb-30">
                                    <select class="form-control" name="tipo">
                                        <option value="" disabled selected hidden>Tipo</option>
                                        <option value="Aperitivos y tapas" >Aperitivos y tapas</option>
                                        <option value="Arroces y cereales" >Arroces y cereales</option>
                                        <option value="Aves" >Aves</option>
                                        <option value="Carnes" >Carnes</option>
                                        <option value="Ensaladas" >Ensaladas</option>
                                        <option value="Guisos y potajes" >Guisos y potajes</option>
                                        <option value="Legumbres" >Legumbres</option>
                                        <option value="Mariscos" >Mariscos</option>
                                        <option value="Pasta" >Pasta</option>
                                        <option value="Pescados" >Pescados</option>
                                        <option value="Postres" >Postres</option>
                                        <option value="Salsa" >Salsa</option>
                                        <option value="Sopas y cremas" >Sopas y cremas</option>
                                        <option value="Verduras" >Verduras</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-12 col-sm-2">
                                <div class="form-group mb-30">
                                    <select class="form-control" name="dificultad">
                                        <option value="" disabled selected hidden>Dificultad</option>
                                        <option value="">Pinche</option>
                                        <option value="">Chef</option>
                                        <option value="">Master Chef</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-sm-2">
                                <div class="form-group mb-30">
                                    <input class="form-control" type="text" name="nombre" placeholder="Ingredient o Nom" >
                                </div>
                            </div>
                            <div class="col-12 col-sm-2">
                                <div class="form-group mb-30">
                                    <button type="submit" name="buscar" class="btn bueno-btn w-100">Buscar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Search Area End ##### -->
    <!-- ##### Hero Area Start ##### -->
    @if ($recomendaciones != null)
    <div class="hero-area">
        <!-- Hero Post Slides -->
        <h2 class="subtemas"><span class="txt anim-text-flow">IA Recommender</span><br>Cree que estas recetas podrian gustarte ;)</h2>
        <div class="hero-post-slides owl-carousel">
            @foreach ($recomendaciones as $recomendacion)
            <!-- Single Slide -->
            <div class="single-slide">
                <!-- Blog Thumbnail -->
                <div class="blog-thumbnail">
                <img src="{{ asset('img/recetas/' . $recomendacion->recetaFoto) }}"/>
                </div>

                <!-- Blog Content -->
                <div class="blog-content-bg">
                    <div class="blog-content">
                        <a href="#" class="post-tag">{{$recomendacion->tipo}}</a>
                        <a href="{{ url('/verReceta/' . $recomendacion->recetaId) }}" class="post-title">{{$recomendacion->nombre}}</a>
                        <div class="post-meta">
                            <a href="#" class="post-date">{{ date('j F, Y', strtotime($recomendacion->created_at)) }}</a>
                            <a href="verUsuario/{{$recomendacion->usuarioId}}" class="post-author">By {{$recomendacion->name}}</a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @endif
    <!-- ##### Hero Area End ##### -->
    <!-- ##### Hero Area Start ##### -->
    <div class="hero-area">
        <!-- Hero Post Slides -->
        <h2 class="subtemas">Recetas recien salidas del horno!</h2>
        <div class="hero-post-slides owl-carousel">
            @foreach ($ultimas as $ultima)
            <!-- Single Slide -->
            <div class="single-slide">
                <!-- Blog Thumbnail -->
                <div class="blog-thumbnail">
                <img src="{{ asset('img/recetas/' . $ultima->fotoReceta) }}" />
                </div>

                <!-- Blog Content -->
                <div class="blog-content-bg">
                    <div class="blog-content">
                        <a href="#" class="post-tag">{{$ultima->tipo}}</a>
                        <a href="{{ url('/verReceta/' . $ultima->recetaId) }}" class="post-title">{{$ultima->nombre}}</a>
                        <div class="post-meta">
                            <a href="#" class="post-date">{{ date('j F, Y', strtotime($ultima->created_at)) }}</a>
                            <a href="verUsuario/{{$ultima->usuarioId}}" class="post-author">By {{$ultima->name}}</a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <!-- ##### Hero Area End ##### -->

    <!-- ##### Big Posts Area Start ##### -->
    <div class="big-posts-area mb-50">
        <h2 class="subtemas">Las recetas que más gustan a nuestra comunidad</h2>
        <div class="container">
            <!-- Single Big Post Area -->
            <div class="row align-items-center">
                <div class="col-12 col-md-6">
                    <div class="big-post-thumbnail mb-50">
                    <a href="{{ url('/verReceta/' . $tresMejores[0]->recetaId) }}"><img class="img-fluid" src="{{ asset('img/recetas/' . $tresMejores[0]->fotoReceta) }}"/></a>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="big-post-content text-center mb-50">
                        <a href="#" class="post-tag">Nº 1</a>
                        <a href="{{ url('/verReceta/' . $tresMejores[0]->recetaId) }}" class="post-title">{{$tresMejores[0]->nombre}}</a>
                        <div class="post-meta">
                            <a href="#" class="post-date">{{ date('j F, Y', strtotime($tresMejores[0]->created_at)) }}</a>
                            <a href="verUsuario/{{$tresMejores[0]->usuarioId}}" class="post-author">By {{$tresMejores[0]->name}}</a>
                        </div>
                        <p>{{$tresMejores[0]->descripcion}}</p>
                        <a href="{{ url('/verReceta/' . $tresMejores[0]->recetaId) }}" class="btn bueno-btn">Ver</a>
                    </div>
                </div>
            </div>

            <!-- Single Big Post Area -->
            <div class="row align-items-center">
                <div class="col-12 col-md-6">
                    <div class="big-post-content text-center mb-50">
                        <a href="#" class="post-tag">Nº 2</a>
                        <a href="{{ url('/verReceta/' . $tresMejores[1]->recetaId) }}" class="post-title">{{$tresMejores[1]->nombre}}</a>
                        <div class="post-meta">
                            <a href="#" class="post-date">{{ date('j F, Y', strtotime($tresMejores[0]->created_at)) }}</a>
                            <a href="verUsuario/{{$tresMejores[1]->usuarioId}}" class="post-author">By {{$tresMejores[1]->name}}</a>
                        </div>
                        <p>{{$tresMejores[1]->descripcion}}</p>
                        <a href="{{ url('/verReceta/' . $tresMejores[1]->recetaId) }}" class="btn bueno-btn">Ver</a>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="big-post-thumbnail mb-50">
                    <a href="{{ url('/verReceta/' . $tresMejores[1]->recetaId) }}"><img class="img-fluid" src="{{ asset('img/recetas/' . $tresMejores[1]->fotoReceta) }}" alt=""></a>
                    </div>
                </div>
            </div>

            <!-- Single Big Post Area -->
            <div class="row align-items-center">
                <div class="col-12 col-md-6">
                    <div class="big-post-thumbnail mb-50">
                        <a href="{{ url('/verReceta/' . $tresMejores[2]->recetaId) }}"><img class="img-fluid" src="{{ asset('img/recetas/' . $tresMejores[2]->fotoReceta) }}"/></a>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="big-post-content text-center mb-50">
                        <a href="#" class="post-tag">Nº 3</a>
                        <a href="{{ url('/verReceta/' . $tresMejores[2]->recetaId) }}" class="post-title">{{$tresMejores[2]->nombre}}</a>
                        <div class="post-meta">
                            <a href="#" class="post-date">{{ date('j F, Y', strtotime($tresMejores[2]->created_at)) }}</a>
                            <a href="verUsuario/{{$tresMejores[2]->usuarioId}}" class="post-author">By {{$tresMejores[2]->name}}</a>
                        </div>
                        <p>{{$tresMejores[2]->descripcion}}</p>
                        <a href="{{ url('/verReceta/' . $tresMejores[2]->recetaId) }}" class="btn bueno-btn">Ver</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="big-posts-area mb-50">
        <div class="row">
            @foreach ($otrasSeis as $seis)
            <!-- Single Best Receipe Area -->
            <div class="col-12 col-sm-6 col-lg-4">
                <div class="single-best-receipe-area mb-30">
                    <div class="img-container">
                    <a href="{{ url('/verReceta/' . $seis->recetaId) }}"><img src="{{ asset('img/recetas/' . $seis->fotoReceta) }}" alt=""></a>
                    </div>
                    <div class="receipe-content">
                        <div class="big-post-content-b">
                            <a href="{{ url('/verReceta/' . $seis->recetaId) }}" class="post-title">{{$seis->nombre}}</a>
                            <div class="ratings">
                                @for ($i = 0; $i < round($seis->media, 0); $i++)
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                 @endfor
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <!-- ##### Big Posts Area End ##### -->

    <!-- ##### Instagram Area Start ##### -->
    <h2 class="subtemas">Entra por los ojos...</h2>
    <div class="instagram-feed-area d-flex flex-wrap">
        @foreach ($fotos as $foto)
        <!-- Single Instagram -->
        <div class="single-instagram">
            <a href="{{ url('/verReceta/' . $foto->id) }}"><img src="{{ asset('img/recetas/' . $foto->fotografia) }}" alt=""></a>
        </div>
        @endforeach
    </div>
    <!-- ##### Instagram Area End ##### -->
    <!-- ##### Footer Area Start ##### -->
    <footer class="footer-area">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-5">
                    <!-- Copywrite Text -->
                    <p class="copywrite-text"><a href="#">
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | by <a href="https://agora.xtec.cat/ies-sabadell/" target="_blank">POGA</a>
                    </p>
                </div>
                <div class="col-12 col-sm-7">
                    <!-- Footer Nav -->
                    <div class="footer-nav">
                        <ul>
                            <li class="active"><a href="{{ url('/..') }}">Home</a></li>
                            <li><a href="#exampleModal" data-toggle="modal" data-target="#exampleModal">About Us</a></li>
                            <li><a href="https://agora.xtec.cat/ies-sabadell/">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">POGA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <div class="modal-body">
            <p>Somos un grupo de alumnos formado por<br><b>Oriol Porta, Pol Garcia e Isaac García</b>. Juntos formamos POGA, sociendad formada para realizar el proyecto final de CFGS Desenvolupaent d'aplicacions Multiplataforma.</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
    </div>
    <!-- ##### Footer Area End ##### -->

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="{{ asset('js/index_recipes/jquery/jquery-2.2.4.min.js')}}"></script>
    <!-- Popper js -->
    <script src="{{ asset('js/index_recipes/bootstrap/popper.min.js')}}"></script>
    <!-- Bootstrap js -->
    <script src="{{ asset('js/index_recipes/bootstrap/bootstrap.min.js')}}"></script>
    <!-- All Plugins js -->
    <script src="{{ asset('js/index_recipes/plugins/plugins.js')}}"></script>
    <!-- Active js -->
    <script src="{{ asset('js/index_recipes/active.js')}}"></script>
    <script src="{{ asset('js/index_recipes/scriptIARecommender.js')}}"></script>
</body>
</html>