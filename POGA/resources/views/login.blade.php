<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login - POGA Receptes &#9679; IA Recommender</title>

    <link rel="stylesheet" href="{{ URL::asset('maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css') }}">
    <!-- Font Icon -->
    <link rel="stylesheet" href="{{ URL::asset('fonts/material-icon/css/material-design-iconic-font.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('jq/login/jquery-ui/jquery-ui.min.css') }}">
    <!-- Favicon -->
    <link rel="icon" href="{{ URL::asset('img/index_recipes/core-img/favicon.ico') }}">
    <!-- Main css -->
    <link rel="stylesheet" href="{{ URL::asset('css/login/styleLogin.css') }}">
</head>
<body>
    <div class="main">
        <section class="signup">
            <h2 class="title">Login<a href="{{ url('/..') }}"> Home</a></h2>
            <div class="container">
                <div class="signup-content">
                    <form method="POST" class="signup-form" id="signup-form" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" value="{{ old('email') }}" class="form-input @error('email') is-invalid @enderror" name="email" id="email" placeholder="example@email.com"/>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-input @error('password') is-invalid @enderror" name="password" id="password"/>
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <a href="{{ url('/registrarse') }}" class="add-info-link">¿No estás registrado?</a>
                        <div class="form-group">
                            <input type="submit" name="submit" id="submit" class="form-submit" value="Submit"/>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
    <!-- JS-->
    <script src="{{URL::asset('ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js')}}"></script>
    <script src="{{URL::asset('jq/login/jquery/jquery.min.js')}}"></script>
    <script src="{{URL::asset('jq/login/jquery-ui/jquery-ui.min.js')}}"></script>
    <script src="{{URL::asset('jq/login/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{URL::asset('jq/login/jquery-validation/dist/additional-methods.min.js')}}"></script>
    <script src="{{URL::asset('js/login/main.js') }}"></script> <!-- Script nombre del fichero seleccionado-->
</body>
</html>