<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Title -->
    <title>{{$receta->nombre}} - POGA Receptes &#9679; IA Recommender</title>

    <!-- Favicon -->
    <link rel="icon" href="{{ URL::asset('img/index_recipes/core-img/favicon.ico') }}">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="{{ URL::asset('css/index_recipes/styleRecipes.css') }}">

</head>

<body>
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>

                    <button type="submit" name="borrar" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                </div>

                <div class="modal-body">
                    <p>Está apunto de borrar esta receta, el proceso será irreversible.</p>
                    <p>¿Está seguro de querer borrarla?</p>
                    <p class="debug-url"></p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <form method="POST" enctype="multipart/form-data" action="{{ route('borrar') }}">
                        @csrf
                        <input type="hidden" name="receta" value="{{$receta->recetaId}}">
                        <button type="submit" class="btn btn-danger btn-ok">Borrar</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="preloader-content">
            <h3>Cooking in progress..</h3>
            <div id="cooking">
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div id="area">
                    <div id="sides">
                        <div id="pan"></div>
                        <div id="handle"></div>
                    </div>
                    <div id="pancake">
                        <div id="pastry"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area">

        <!-- Top Header Area -->
        <div class="top-header-area bg-img bg-overlay">
            <div class="container h-100">
                <div class="row h-100 align-items-center justify-content-between">
                    <div class="col-12 col-sm-6">
                        <!-- Top Social Info -->
                        <div class="top-social-info">
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Pinterest"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-5 col-xl-4">
                        <!-- Top Search Area -->
                        <div class="top-search-area">
                            <form action="#" method="post">
                                <input type="search" name="top-search" id="topSearch" placeholder="Search">
                                <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Logo Area -->
        <div class="logo-area">
            <a href="{{ url('/..') }}"><img src="{{URL::asset('img/index_recipes/core-img/poga3.png')}}" alt=""></a>
        </div>

        <!-- Navbar Area -->
        <div class="bueno-main-menu" id="sticker">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Menu -->
                    <nav class="classy-navbar justify-content-between" id="buenoNav">

                        <!-- Toggler -->
                        <div id="toggler"><img src="{{URL::asset('img/index_recipes/core-img/toggler.png')}}" alt=""></div>

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">

                            <!-- Close Button -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                    <li><a href="{{ url('/..') }}">Home</a></li>
                                    <li><a href="#exampleModal" data-toggle="modal" data-target="#exampleModal">About Us</a></li>
                                    <li><a href="https://agora.xtec.cat/ies-sabadell/">Contact</a></li>
                                </ul>

                                <!-- Login/Register -->
                                @if( Auth::guest() )
                                <div class="login-area">
                                    <a href="{{ url('/logear') }}">Login</a> / <a href="{{ url('/registrarse') }}">Registrar</a>
                                </div>
                                @else
                                <ul class="ullogin">
                                    <li><a href="#"><i class="fa fa fa-user"></i> &nbsp;{{$usuario->name}}</a>
                                        <ul class="dropdown">
                                            <li><a href="{{ url('/verUsuario/' . $usuario->id) }}"><i class="fa fa-user"></i> &nbsp;Area Personal</a></li>
                                            <li><a href="{{ url('/insertReceta') }}"><i class="fa fa-plus"></i> &nbsp;Nueva Receta</a></li>
                                            <li class="salir"><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> &nbsp;Logout</a></li>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                                        </ul>
                                    </li>
                                </ul>
                                @endif
                            </div>
                            <!-- Nav End -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ##### Header Area End ##### -->
    <!-- ##### Treading Post Area Start ##### -->
    <div class="treading-post-area" id="treadingPost">
        <div class="close-icon">
            <i class="fa fa-times"></i>
        </div>

        <h4>Treading Post</h4>

        <!-- Single Blog Post -->
        <div class="single-blog-post style-1 d-flex flex-wrap mb-30">
            <!-- Blog Thumbnail -->
            <div class="blog-thumbnail">
                <img src="{{URL::asset('img/index_recipes/bg-img/9.jpg')}}" alt="">
            </div>
            <!-- Blog Content -->
            <div class="blog-content">
                <a href="#" class="post-tag">The Best</a>
                <a href="#" class="post-title">Friend eggs with ham</a>
                <div class="post-meta">
                    <a href="#" class="post-date">July 11, 2018</a>
                    <a href="#" class="post-author">By Julia Stiles</a>
                </div>
            </div>
        </div>

        <!-- Single Blog Post -->
        <div class="single-blog-post style-1 d-flex flex-wrap mb-30">
            <!-- Blog Thumbnail -->
            <div class="blog-thumbnail">
                <img src="{{URL::asset('img/index_recipes/bg-img/10.jpg')}}" alt="">
            </div>
            <!-- Blog Content -->
            <div class="blog-content">
                <a href="#" class="post-tag">The Best</a>
                <a href="#" class="post-title">Mushrooms with pork chop</a>
                <div class="post-meta">
                    <a href="#" class="post-date">July 11, 2018</a>
                    <a href="#" class="post-author">By Julia Stiles</a>
                </div>
            </div>
        </div>

        <!-- Single Blog Post -->
        <div class="single-blog-post style-1 d-flex flex-wrap mb-30">
            <!-- Blog Thumbnail -->
            <div class="blog-thumbnail">
                <img src="{{URL::asset('img/index_recipes/bg-img/11.jpg')}}" alt="">
            </div>
            <!-- Blog Content -->
            <div class="blog-content">
                <a href="#" class="post-tag">The Best</a>
                <a href="#" class="post-title">Birthday cake with chocolate</a>
                <div class="post-meta">
                    <a href="#" class="post-date">July 11, 2018</a>
                    <a href="#" class="post-author">By Julia Stiles</a>
                </div>
            </div>
        </div>

        <!-- Single Blog Post -->
        <div class="single-blog-post style-1 d-flex flex-wrap mb-30">
            <!-- Blog Thumbnail -->
            <div class="blog-thumbnail">
                <img src="{{URL::asset('img/index_recipes/bg-img/9.jpg')}}" alt="">
            </div>
            <!-- Blog Content -->
            <div class="blog-content">
                <a href="#" class="post-tag">The Best</a>
                <a href="#" class="post-title">Friend eggs with ham</a>
                <div class="post-meta">
                    <a href="#" class="post-date">July 11, 2018</a>
                    <a href="#" class="post-author">By Julia Stiles</a>
                </div>
            </div>
        </div>

        <!-- Single Blog Post -->
        <div class="single-blog-post style-1 d-flex flex-wrap mb-30">
            <!-- Blog Thumbnail -->
            <div class="blog-thumbnail">
                <img src="{{URL::asset('img/index_recipes/bg-img/10.jpg')}}" alt="">
            </div>
            <!-- Blog Content -->
            <div class="blog-content">
                <a href="#" class="post-tag">The Best</a>
                <a href="#" class="post-title">Mushrooms with pork chop</a>
                <div class="post-meta">
                    <a href="#" class="post-date">July 11, 2018</a>
                    <a href="#" class="post-author">By Julia Stiles</a>
                </div>
            </div>
        </div>

        <!-- Single Blog Post -->
        <div class="single-blog-post style-1 d-flex flex-wrap mb-30">
            <!-- Blog Thumbnail -->
            <div class="blog-thumbnail">
                <img src="{{URL::asset('img/index_recipes/bg-img/11.jpg')}}" alt="">
            </div>
            <!-- Blog Content -->
            <div class="blog-content">
                <a href="#" class="post-tag">The Best</a>
                <a href="#" class="post-title">Birthday cake with chocolate</a>
                <div class="post-meta">
                    <a href="#" class="post-date">July 11, 2018</a>
                    <a href="#" class="post-author">By Julia Stiles</a>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Treading Post Area End ##### -->

    <!-- ##### Post Details Area Start ##### -->
    <section class="post-news-area section-padding-100-0 mb-30">
        <div class="container">
            <div class="row justify-content-center">
                <!-- Post Details Content Area -->
                <div class="col-12 col-lg-4 col-xl-5">
                    <div class="post-details-content mb-100">
                        <div class="blog-thumbnail mb-50">
                        <a href="{{ asset('img/recetas/' . $receta->recetaFoto) }}" class="img-zoom" title="{{$receta->nombre}}">
                            <img src="{{ asset('img/recetas/' . $receta->recetaFoto) }}" />
                        </a>
                        </div>
                        <div class="blog-content">
                            <a href="#" class="post-tag">{{$receta->tipo}}</a>
                            <h4 class="post-title">{{$receta->nombre}}</h4>
                            <div class="post-meta mb-50">
                                <a href="#" class="post-date">{{ date('j F, Y', strtotime($receta->created_at)) }}</a>
                                <a href="../verUsuario/{{$receta->usuarioId}}" class="post-author">By {{$receta->name}}</a>
                                <p>{{$receta->descripcion}}</p>
                            </div>
                            @foreach ($pasos as $pas)
                            <div>
                                <div id="circulo">
                                    <h5 >{{$pas['numero']}}</h5>
                                </div>
                                <p>{{$pas['descripcion']}}</p>
                            </div>
                            <br>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4 col-xl-4">
                    <!-- Info -->
                    <div class="recipe-info">
                        <h5>Info</h5>
                        <ul class="info-data">
                            <li>
                            @if(!Auth::guest())
                                <button onclick="like({{$receta->recetaId}})">
                                @if ($valoracion === [] || $valoracion[0]->favorito == 0)
                                <img class="ico-like" id ="heart-plus" src="{{ asset('img/index_recipes/core-img/heart-plus-outline.svg')}}"></button>
                                @else
                                <img class="ico-like" id ="heart-remove" src="{{ asset('img/index_recipes/core-img/heart-remove.svg')}}"></button>
                                @endif
                                @if ($usuario->id == $receta->usuarioId)
                                <span><a href="{{ url('/updateRecetaForm/' . $receta->recetaId) }}"><svg class="ico-edit" style="width:24px;height:24px" viewBox="0 0 24 24">
                                <path d="M8,12H16V14H8V12M10,20H6V4H13V9H18V12.1L20,10.1V8L14,2H6A2,2 0 0,0 4,4V20A2,2 0 0,0 6,22H10V20M8,18H12.1L13,17.1V16H8V18M20.2,13C20.3,13 20.5,13.1 20.6,13.2L21.9,14.5C22.1,14.7 22.1,15.1 21.9,15.3L20.9,16.3L18.8,14.2L19.8,13.2C19.9,13.1 20,13 20.2,13M20.2,16.9L14.1,23H12V20.9L18.1,14.8L20.2,16.9Z" />
                                </svg></a></span><span>
                                <button data-toggle="modal" data-target="#confirm-delete"><svg class="ico-delete" style="width:24px;height:24px" viewBox="0 0 24 24"><path d="M9,3V4H4V6H5V19A2,2 0 0,0 7,21H17A2,2 0 0,0 19,19V6H20V4H15V3H9M7,6H17V19H7V6M9,8V17H11V8H9M13,8V17H15V8H13Z" />
                                </svg></button></span><span>
                                @else
                                <span>
                                @endif
                            @endif
                            <button onclick="javascript:window.print()"><svg class="ico-print" style="width:24px;height:24px" viewBox="0 0 24 24">
                             <path d="M19 8C20.66 8 22 9.34 22 11V17H18V21H6V17H2V11C2 9.34 3.34 8 5 8H6V3H18V8H19M8 5V8H16V5H8M16 19V15H8V19H16M18 15H20V11C20 10.45 19.55 10 19 10H5C4.45 10 4 10.45 4 11V15H6V13H18V15M19 11.5C19 12.05 18.55 12.5 18 12.5C17.45 12.5 17 12.05 17 11.5C17 10.95 17.45 10.5 18 10.5C18.55 10.5 19 10.95 19 11.5Z" />
                            </svg></button></span></li>
                        </ul>

                        <ul class="info-data">
                            <div class="wrapper">
                                <ul>
                                    <li id="star-5" onclick="puntuar({{$receta->recetaId}}, 5)">★</li>
                                    <li id="star-4" onclick="puntuar({{$receta->recetaId}}, 4)">★</li>
                                    <li id="star-3" onclick="puntuar({{$receta->recetaId}}, 3)">★</li>
                                    <li id="star-2" onclick="puntuar({{$receta->recetaId}}, 2)">★</li>
                                    <li id="star-1" onclick="puntuar({{$receta->recetaId}}, 1)">★</li>
                                </ul>
                            </div>
                            <!-- <li><img src="{{URL::asset('img/index_recipes/core-img/star-outline.png')}}" alt=""><span><img onclick="puntuar({{$receta->recetaId}}, 1)" src="{{URL::asset('img/index_recipes/core-img/star.png')}}" alt=""><img src="{{URL::asset('img/index_recipes/core-img/star.png')}}" alt=""><img src="{{URL::asset('img/index_recipes/core-img/star.png')}}" alt=""><img src="{{URL::asset('img/index_recipes/core-img/star-half-full.png')}}" alt=""></span></li> -->
                            <li><img src="{{URL::asset('img/index_recipes/core-img/alarm-clock.png')}}" alt=""> <span>{{$receta->duracion}} min</span></li>
                            <li><img src="{{URL::asset('img/index_recipes/core-img/tray.png')}}" alt=""> <span>{{$receta->personas}}</span></li>
                            <li><img src="{{URL::asset('img/index_recipes/core-img/sandwich.png')}}" alt=""> <span>{{$receta->dificultad}}</span></li>
                            <li><img src="{{URL::asset('img/index_recipes/core-img/compass.png')}}" alt=""> <span>{{$receta->origen}}</span></li>
                        </ul>
                    </div>
                    @if(!Auth::guest())
                    <form method="POST" enctype="multipart/form-data" action="{{ route('comentar') }}" id="form">
                        @csrf
                        <input type="hidden" value="{{$receta->recetaId}}" name="id">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border"><i class="fa fa-comment"></i></legend>
                            <textarea onKeyUp="limitarLargo(this,280);" class="form-control" placeholder="Haz click aquí para hacer llegar tu opinión a {{$receta->name}} (Max 250)" name="comentario"></textarea>
                            <br><button type="submit" name="subida" class="btn btn-outline-success btn-lg btn-block">Enviar comentario</button>
                        </fieldset>
                    </form>
                    @endif
                    <br>
                    <!-- Ingredients -->
                    <div class="ingredients">
                        <h5>Ingredientes</h5>
                        <!-- Custom Checkbox -->
                        @php
                            $i = 0;
                            $customCheck = "customCheck"
                        @endphp
                        @foreach ($ingredientes as $int)
                            @php
                                $i++
                            @endphp
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="{{$customCheck.$i}}">
                                <label class="custom-control-label" for="{{$customCheck.$i}}">{{$int['cantidad']}} {{$int['nombre']}}</label>
                            </div>
                        @endforeach
                    </div>
                </div>

                <!-- Sidebar Widget -->
                <div class="col-12 col-sm-9 col-md-6 col-lg-4 col-xl-3">
                    <div class="sidebar-area">

                        <!-- Single Widget Area -->
                        <div class="single-widget-area author-widget mb-30">
                            <div class="background-pattern bg-img">
                                <div class="author-thumbnail">
                                    <img src="{{ asset('img/usuarios/' . $receta->usuarioFoto) }}" alt="">
                                </div>
                                <p>My name is <span>{{$receta->name}}</span>, I’m a passionate cook with a love food</p>
                            </div>
                            <div class="social-info">
                                <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            </div>
                        </div>

                        <!-- Single Widget Area -->
                        <div class="single-widget-area add-widget mb-30">
                            <img src="{{URL::asset('img/index_recipes/bg-img/add.png')}}" alt="">
                        </div>

                        <!-- Single Widget Area -->
                        <div class="single-widget-area post-widget mb-30">
                            <!-- Single Post Area -->
                            <div class="single-post-area d-flex">
                                <!-- Blog Thumbnail -->
                                <div class="blog-thumbnail">
                                    <img src="{{URL::asset('img/index_recipes/bg-img/12.jpg')}}" alt="">
                                </div>
                                <!-- Blog Content -->
                                <div class="blog-content">
                                    <a href="#" class="post-title">Friend eggs with ham</a>
                                    <div class="post-meta">
                                        <a href="#" class="post-date">July 11, 2018</a>
                                        <a href="#" class="post-author">By Julia Stiles</a>
                                    </div>
                                </div>
                            </div>

                            <!-- Single Post Area -->
                            <div class="single-post-area d-flex">
                                <!-- Blog Thumbnail -->
                                <div class="blog-thumbnail">
                                    <img src="{{URL::asset('img/index_recipes/bg-img/13.jpg')}}" alt="">
                                </div>
                                <!-- Blog Content -->
                                <div class="blog-content">
                                    <a href="#" class="post-title">Burger with fries</a>
                                    <div class="post-meta">
                                        <a href="#" class="post-date">July 11, 2018</a>
                                        <a href="#" class="post-author">By Julia Stiles</a>
                                    </div>
                                </div>
                            </div>

                            <!-- Single Post Area -->
                            <div class="single-post-area d-flex">
                                <!-- Blog Thumbnail -->
                                <div class="blog-thumbnail">
                                    <img src="{{URL::asset('img/index_recipes/bg-img/14.jpg')}}" alt="">
                                </div>
                                <!-- Blog Content -->
                                <div class="blog-content">
                                    <a href="#" class="post-title">Avocado &amp; Oisters</a>
                                    <div class="post-meta">
                                        <a href="#" class="post-date">July 11, 2018</a>
                                        <a href="#" class="post-author">By Julia Stiles</a>
                                    </div>
                                </div>
                            </div>

                            <!-- Single Post Area -->
                            <div class="single-post-area d-flex">
                                <!-- Blog Thumbnail -->
                                <div class="blog-thumbnail">
                                    <img src="{{URL::asset('img/index_recipes/bg-img/15.jpg')}}" alt="">
                                </div>
                                <!-- Blog Content -->
                                <div class="blog-content">
                                    <a href="#" class="post-title">Tortilla prawns</a>
                                    <div class="post-meta">
                                        <a href="#" class="post-date">July 11, 2018</a>
                                        <a href="#" class="post-author">By Julia Stiles</a>
                                    </div>
                                </div>
                            </div>

                            <!-- Single Post Area -->
                            <div class="single-post-area d-flex">
                                <!-- Blog Thumbnail -->
                                <div class="blog-thumbnail">
                                    <img src="{{URL::asset('img/index_recipes/bg-img/16.jpg')}}" alt="">
                                </div>
                                <!-- Blog Content -->
                                <div class="blog-content">
                                    <a href="#" class="post-title">Burger with fries</a>
                                    <div class="post-meta">
                                        <a href="#" class="post-date">July 11, 2018</a>
                                        <a href="#" class="post-author">By Julia Stiles</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Post Details Area End ##### -->
    <!-- ##### Recipe quotes ##### -->
    <div class="hero-area">
        <div class="hero-post-slides owl-carousel">
            @foreach ($comentarios as $comentario)
            <!-- Single Slide -->
            <div class="single-slide">
                <!-- Blog Thumbnail -->
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border"><i class="fa fa-quote-left"></i></legend>
                    <div class="testimonial__item">
                                <div class="testimonial__author">
                                    <div class="testimonial__author__pic">
                                        <img src="{{ asset('img/usuarios/' . $comentario->fotografia) }}" alt="">
                                    </div>
                                    <div class="testimonial__author__text">
                                        <h5>{{$comentario->name}}</h5>
                                        <span>{{ date('j F, Y', strtotime($comentario->updated_at)) }}</span>
                                    </div>
                                    <div class="rating">
                                    @for ($i = 0; $i < $comentario->puntuacion; $i++)
                                        <span class="fa fa-star"></span>
                                    @endfor
                                </div>
                                </div>
                                <p>{{$comentario->opinion}}</p>
                            </div>
                </fieldset>
            </div>
            @endforeach
        </div>
    </div>
    <!-- ##### Footer Area Start ##### -->
    <footer class="footer-area">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-5">
                    <!-- Copywrite Text -->
                    <p class="copywrite-text"><a href="#">
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | by <a href="https://agora.xtec.cat/ies-sabadell/" target="_blank">POGA</a>
                    </p>
                </div>
                <div class="col-12 col-sm-7">
                    <!-- Footer Nav -->
                    <div class="footer-nav">
                        <ul>
                            <li class="active"><a href="{{ url('/..') }}">Home</a></li>
                            <li><a href="#exampleModal" data-toggle="modal" data-target="#exampleModal">About Us</a></li>
                            <li><a href="https://agora.xtec.cat/ies-sabadell/">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">POGA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <div class="modal-body">
            <p>Somos un grupo de alumnos formado por<br><b>Oriol Porta, Pol Garcia e Isaac García</b>. Juntos formamos POGA, sociendad formada para realizar el proyecto final de CFGS Desenvolupaent d'aplicacions Multiplataforma.</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
    </div>
    <!-- ##### Footer Area End ##### -->
    <script>
        $('#confirm-delete').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));

            $('.debug-url').html('Delete URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
        });
    </script>
     <script>
        function limitarLargo(campo, maximo) {
        if(campo.value.length > maximo)
        campo.value= campo.value.substring(0, maximo);
        }
    </script>
    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="{{URL::asset('https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js')}}"></script>
    <script src="{{ asset('js/valoraciones/like.js')}}"></script>
    <script src="{{ asset('js/index_recipes/jquery/jquery-2.2.4.min.js')}}"></script>
    <!-- Popper js -->
    <script src="{{ asset('js/index_recipes/bootstrap/popper.min.js')}}"></script>
    <!-- Bootstrap js -->
    <script src="{{ asset('js/index_recipes/bootstrap/bootstrap.min.js')}}"></script>
    <!-- All Plugins js -->
    <script src="{{ asset('js/index_recipes/plugins/plugins.js')}}"></script>
    <!-- Active js -->
    <script src="{{ asset('js/index_recipes/active.js')}}"></script>
</body>
</html>