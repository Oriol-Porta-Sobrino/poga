<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
     <link rel="stylesheet" href="{{ URL::asset('css/index_recipes/styleNewRecipes.css') }}">
    </script>
</head>
<body onload="carga({{$pasos}}, {{$ingredientes}})">

    <form method="POST" enctype="multipart/form-data" action="{{ route('updateReceta') }}" id="form">
        @csrf
        <input type="hidden" name="id" value="{{$receta['id']}}">
        <label>Nombre</label>
        <input type="text" name="nombre" value="{{$receta['nombre']}}">
        @error('nombre')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
        <label>Origen</label>
        <select name="origen">
            <option value="" selected disabled hidden>Escoge origen</option>
            <option @if ($receta['origen'] == 'Europea') selected @endif value="Europea">Europea</option>
            <option @if ($receta['origen'] == 'Asiatica') selected @endif value="Asiatica">Asiatica</option>
            <option @if ($receta['origen'] == 'Norteamericana') selected @endif value="Norteamericana">Norteamericana</option>
            <option @if ($receta['origen'] == 'Sudamericana') selected @endif value="Sudamericana">Sudamericana</option>
            <option @if ($receta['origen'] == 'Oceanica') selected @endif value="Oceanica">Oceanica</option>
            <option @if ($receta['origen'] == 'Africana') selected @endif value="Africana">Africana</option>
        </select>
        <label>Dificultad</label>
        <select name="dificultad">
            <option value="" selected disabled hidden>Escoge dificultad</option>
            <option @if ($receta['dificultad'] == 'Pinche') selected @endif value="Pinche">Pinche</option>
            <option @if ($receta['dificultad'] == 'Chef') selected @endif value="Chef">Chef</option>
            <option @if ($receta['dificultad'] == 'Masterchef') selected @endif value="Masterchef">Masterchef</option>
        </select>
        <label>Tipo</label>
        <select name="tipo">
            <option value="" selected disabled hidden>Escoge tipo</option>
            <option @if ($receta['tipo'] == 'Aperitivos y tapas') selected @endif value="Aperitivos y tapas" >Aperitivos y tapas</option>
            <option @if ($receta['tipo'] == 'Arroces y cereales') selected @endif value="Arroces y cereales" >Arroces y cereales</option>
            <option @if ($receta['tipo'] == 'Aves') selected @endif value="Aves" >Aves</option>
            <option @if ($receta['tipo'] == 'Carnes') selected @endif value="Carnes" >Carnes</option>
            <option @if ($receta['tipo'] == 'Ensaladas') selected @endif value="Ensaladas" >Ensaladas</option>
            <option @if ($receta['tipo'] == 'Guisos y potajes') selected @endif value="Guisos y potajes" >Guisos y potajes</option>
            <option @if ($receta['tipo'] == 'Legumbres') selected @endif value="Legumbres" >Legumbres</option>
            <option @if ($receta['tipo'] == 'Mariscos') selected @endif value="Mariscos" >Mariscos</option>
            <option @if ($receta['tipo'] == 'Pescados') selected @endif value="Pescados" >Pescados</option>
            <option @if ($receta['tipo'] == 'Postres') selected @endif value="Postres" >Postres</option>
            <option @if ($receta['tipo'] == 'Pasta') selected @endif value="Pasta" >Pasta</option>
            <option @if ($receta['tipo'] == 'Salsa') selected @endif value="Salsa" >Salsa</option>
            <option @if ($receta['tipo'] == 'Sopas y cremas') selected @endif value="Sopas y cremas" >Sopas y cremas</option>
            <option @if ($receta['tipo'] == 'Verduras') selected @endif value="Verduras" >Verduras</option>
        </select>
        <label>Duracion</label>
        <input type="number" name="duracion" value="{{$receta['duracion']}}">
        <label>Personas</label>
        <input type="number" name="personas" value="{{$receta['personas']}}">
        <label>Descripcion</label>
        <input type="text" name="descripcion" value="{{$receta['descripcion']}}">
        <label>Fotografia</label>
        <input type="file" name="fotografia">
        <input type="submit" name="subida">
        <div id="pasos"></div>
        <div id="ingredientes"></div>
    </form>

    <button onclick="addPaso()">Añadir paso</button>
    <button onclick="addIngrediente()">Añadir ingrediente</button>


    <script src="{{ asset('js/updateReceta/addInputs.js')}}"></script>

</body>
</html>