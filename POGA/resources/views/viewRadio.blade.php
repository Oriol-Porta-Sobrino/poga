<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
    </script>
</head>
<body>
    
    <form method="POST" enctype="multipart/form-data" action="{{ route('anadirReceta') }}" id="form">
        @csrf
        <label>Nombre</label>
        <input type="text" name="nombre">
        @error('nombre')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
        <input list="browsers" name="browsers">

        <datalist id="browsers">
        <option value="Internet Explorer">
        <option value="Firefox">
        <option value="Google Chrome">
        <option value="Opera">
        <option value="Safari">
        </datalist>
        <label>Origen</label>
        <input name="origen" list="origen">
        <datalist id="origen">
            <option value="Europea">
            <option value="Asiatica">
        <input type="radio" name="origen" id="europea" value="Europea">
        <label for="europea">Europea</label>
        <input type="radio" name="origen" id="asiatica" value="Asiatica">
        <label for="asiatica">Asiatica</label>
        <label>Dificultad</label>
        <input type="radio" name="dificultad" id="pinche" value="Pinche">
        <label for="pinche">Pinche</label>
        <input type="radio" name="dificultad" id="chef" value="Chef">
        <label for="chef">Chef</label>
        <input type="radio" name="dificultad" id="masterchef" value="Masterchef">
        <label for="masterchef">Masterchef</label>
        <label>Tipo</label>
        <input type="radio" name="tipo" id="Aperitivos y tapas" value="Aperitivos y tapas">
        <label for="Aperitivos y tapas" >Aperitivos y tapas</label>
        <input type="radio" name="tipo" id="Arroces y cereales" value="Arroces y cereales">
        <label for="Arroces y cereales">Arroces y cereales</label>
        <input type="radio" name="tipo" id="Aves" value="Aves">
        <label for="Aves">Aves</label>
        <input type="radio" name="tipo" id="Carnes" value="Carnes">
        <label for="Carnes">Carnes</label>
        <input type="radio" name="tipo" id="Ensaladas" value="Ensaladas">
        <label for="Ensaladas">Ensaladas</label>
        <input type="radio" name="tipo" id="Guisos y potajes" value="Guisos y potajes">
        <label for="Guisos y potajes">Guisos y potajes</label>
        <input type="radio" name="tipo" id="Legumbres" value="Legumbres">
        <label for="Legumbres">Legumbres</label>
        <input type="radio" name="tipo" id="Mariscos" value="Mariscos">
        <label for="Mariscos">Mariscos</label>
        <input type="radio" name="tipo" id="Pescados" value="Pescados">
        <label for="Pescados">Pescados</label>
        <input type="radio" name="tipo" id="Postres" value="Postres">
        <label for="Postres">Postres</label>
        <input type="radio" name="tipo" id="Pasta" value="Pasta">
        <label for="Pasta">Pasta</label>
        <input type="radio" name="tipo" id="Salsa" value="Salsa">
        <label for="Salsa">Salsa</label>
        <input type="radio" name="tipo" id="Sopas y cremas" value="Sopas y cremas">
        <label for="Sopas y cremas">Sopas y cremas</label>
        <input type="radio" name="tipo" id="Verduras" value="Verduras">
        <label for="Verduras">Verduras</label>
        <label>Duracion</label>
        <input type="number" name="duracion">
        <label>Personas</label>
        <input type="number" name="personas">
        <label>Descripcion</label>
        <input type="text" name="descripcion">
        <label>Fotografia</label>
        <input type="file" name="fotografia">
        <input type="submit" name="subida">
        <div id="pasos"></div>
        <div id="ingredientes"></div>
    </form>
    
    <button onclick="addPaso()">Añadir paso</button>
    <button onclick="addIngrediente()">Añadir ingrediente</button>
    

    <script src="{{ asset('js/insertarReceta/paso.js')}}"></script>
    <script src="{{ asset('js/insertarReceta/ingrediente.js')}}"></script>
</body>
</html>