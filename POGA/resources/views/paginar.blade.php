<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <title>Busqueda - POGA Receptes &#9679; IA Recommender</title>

    <!-- Favicon -->
    <link rel="icon" href="{{ URL::asset('img/core-img/favicon.ico') }}">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="{{ URL::asset('css/index_recipes/style.css') }}">
    <!-- <link rel="stylesheet" href="{{ URL::asset('maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css') }}"> -->
    <link rel="stylesheet" href="{{ URL::asset('//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css') }}">

</head>
<body>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="preloader-content">
            <h3>Cooking in progress..</h3>
            <div id="cooking">
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div id="area">
                    <div id="sides">
                        <div id="pan"></div>
                        <div id="handle"></div>
                    </div>
                    <div id="pancake">
                        <div id="pastry"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area">
        <!-- Top Header Area -->
        <div class="top-header-area bg-img bg-overlay">
            <div class="container h-100">
                <div class="row h-100 align-items-center justify-content-between">
                    <div class="col-12 col-sm-6">
                        <!-- Top Social Info -->
                        <div class="top-social-info">
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Pinterest"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-5 col-xl-4">
                        <!-- Top Search Area -->
                        <div class="top-search-area">
                            <form method="GET" enctype="multipart/form-data" action="{{ route('buscar') }}">
                                @csrf
                                <input type="search" name="nombre" id="topSearch" placeholder="Search">
                                <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Logo Area -->
        <div class="logo-area">
            <a href="{{ url('/..') }}"><img src="{{URL::asset('img/index_recipes/core-img/poga3.png')}}" alt=""></a>
        </div>

        <!-- Navbar Area -->
        <div class="bueno-main-menu" id="sticker">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Menu -->
                    <nav class="classy-navbar justify-content-between" id="buenoNav">

                        <!-- Toggler -->
                        <div id="toggler"><img src="{{URL::asset('img/index_recipes/core-img/toggler.png')}}" alt=""></div>

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">

                            <!-- Close Button -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                    <li><a href="{{ url('/..') }}">Home</a></li>
                                    <li><a href="#exampleModal" data-toggle="modal" data-target="#exampleModal">About Us</a></li>
                                    <li><a href="https://agora.xtec.cat/ies-sabadell/">Contact</a></li>
                                </ul>

                                <!-- Login/Register -->
                                @if( Auth::guest() )
                                <div class="login-area">
                                    <a href="{{ url('/logear') }}">Login</a> / <a href="{{ url('/registrarse') }}">Registrar</a>
                                </div>
                                @else
                                <ul class="ullogin">
                                    <li><a href="#"><i class="fa fa fa-user"></i> &nbsp;{{$usuario->name}}</a>
                                        <ul class="dropdown">
                                            <li><a href="{{ url('/verUsuario/' . $usuario->id) }}"><i class="fa fa-user"></i> &nbsp;Area Personal</a></li>
                                            <li><a href="{{ url('/insertReceta') }}"><i class="fa fa-plus"></i> &nbsp;Nueva Receta</a></li>
                                            <li class="salir"><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> &nbsp;Logout</a></li>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                                        </ul>
                                    </li>
                                </ul>
                                @endif
                            </div>
                            <!-- Nav End -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ##### Treading Post Area Start ##### -->
    <div class="treading-post-area" id="treadingPost">
        <div class="close-icon">
            <i class="fa fa-times"></i>
        </div>

        <h4>Treading Post</h4>

        <!-- Single Blog Post -->
        <div class="single-blog-post style-1 d-flex flex-wrap mb-30">
            <!-- Blog Thumbnail -->
            <div class="blog-thumbnail">
                <img src="{{ URL::asset('img/index_recipes/bg-img/9.jpg') }}" alt="">
            </div>
            <!-- Blog Content -->
            <div class="blog-content">
                <a href="#" class="post-tag">The Best</a>
                <a href="#" class="post-title">Friend eggs with ham</a>
                <div class="post-meta">
                    <a href="#" class="post-date">July 11, 2018</a>
                    <a href="#" class="post-author">By Julia Stiles</a>
                </div>
            </div>
        </div>

        <!-- Single Blog Post -->
        <div class="single-blog-post style-1 d-flex flex-wrap mb-30">
            <!-- Blog Thumbnail -->
            <div class="blog-thumbnail">
            <img src="{{ URL::asset('img/index_recipes/bg-img/10.jpg') }}" alt="">
            </div>
            <!-- Blog Content -->
            <div class="blog-content">
                <a href="#" class="post-tag">The Best</a>
                <a href="#" class="post-title">Mushrooms with pork chop</a>
                <div class="post-meta">
                    <a href="#" class="post-date">July 11, 2018</a>
                    <a href="#" class="post-author">By Julia Stiles</a>
                </div>
            </div>
        </div>

        <!-- Single Blog Post -->
        <div class="single-blog-post style-1 d-flex flex-wrap mb-30">
            <!-- Blog Thumbnail -->
            <div class="blog-thumbnail">
                <img src="img/index_recipes/bg-img/11.jpg" alt="">
            </div>
            <!-- Blog Content -->
            <div class="blog-content">
                <a href="#" class="post-tag">The Best</a>
                <a href="#" class="post-title">Birthday cake with chocolate</a>
                <div class="post-meta">
                    <a href="#" class="post-date">July 11, 2018</a>
                    <a href="#" class="post-author">By Julia Stiles</a>
                </div>
            </div>
        </div>

        <!-- Single Blog Post -->
        <div class="single-blog-post style-1 d-flex flex-wrap mb-30">
            <!-- Blog Thumbnail -->
            <div class="blog-thumbnail">
                <img src="img/index_recipes/bg-img/9.jpg" alt="">
            </div>
            <!-- Blog Content -->
            <div class="blog-content">
                <a href="#" class="post-tag">The Best</a>
                <a href="#" class="post-title">Friend eggs with ham</a>
                <div class="post-meta">
                    <a href="#" class="post-date">July 11, 2018</a>
                    <a href="#" class="post-author">By Julia Stiles</a>
                </div>
            </div>
        </div>

        <!-- Single Blog Post -->
        <div class="single-blog-post style-1 d-flex flex-wrap mb-30">
            <!-- Blog Thumbnail -->
            <div class="blog-thumbnail">
                <img src="img/index_recipes/bg-img/10.jpg" alt="">
            </div>
            <!-- Blog Content -->
            <div class="blog-content">
                <a href="#" class="post-tag">The Best</a>
                <a href="#" class="post-title">Mushrooms with pork chop</a>
                <div class="post-meta">
                    <a href="#" class="post-date">July 11, 2018</a>
                    <a href="#" class="post-author">By Julia Stiles</a>
                </div>
            </div>
        </div>

        <!-- Single Blog Post -->
        <div class="single-blog-post style-1 d-flex flex-wrap mb-30">
            <!-- Blog Thumbnail -->
            <div class="blog-thumbnail">
                <img src="img/index_recipes/bg-img/11.jpg" alt="">
            </div>
            <!-- Blog Content -->
            <div class="blog-content">
                <a href="#" class="post-tag">The Best</a>
                <a href="#" class="post-title">Birthday cake with chocolate</a>
                <div class="post-meta">
                    <a href="#" class="post-date">July 11, 2018</a>
                    <a href="#" class="post-author">By Julia Stiles</a>
                </div>
            </div>
        </div>
    </div>
      <!-- ##### Search Area Start ##### -->
      <div class="bueno-search-area section-padding-100-0 pb-70">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <form method="GET" enctype="multipart/form-data" action="{{ route('filtrar') }}" id="form">
                        @csrf
                        <div class="row">
                            <div class="col-12 col-sm-3">
                                <div class="form-group mb-30">
                                    <select class="form-control" name="origen">
                                        <option value="" disabled selected hidden>Origen</option>
                                        <option value="Europea">Europea</option>
                                        <option value="Asiatica">Asiatica</option>
                                        <option value="Norte-Americana">Norte-Americana</option>
                                        <option value="Sud-Americana">Sud-Americana</option>
                                        <option value="Oceanica">Oceanica</option>
                                        <option value="Africana">Africana</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-12 col-sm-3">
                                <div class="form-group mb-30">
                                    <select class="form-control" name="tipo">
                                        <option value="" disabled selected hidden>Tipo</option>
                                        <option value="Aperitivos y tapas" >Aperitivos y tapas</option>
                                        <option value="Arroces y cereales" >Arroces y cereales</option>
                                        <option value="Aves" >Aves</option>
                                        <option value="Carnes" >Carnes</option>
                                        <option value="Ensaladas" >Ensaladas</option>
                                        <option value="Guisos y potajes" >Guisos y potajes</option>
                                        <option value="Legumbres" >Legumbres</option>
                                        <option value="Mariscos" >Mariscos</option>
                                        <option value="Pasta" >Pasta</option>
                                        <option value="Pescados" >Pescados</option>
                                        <option value="Postres" >Postres</option>
                                        <option value="Salsa" >Salsa</option>
                                        <option value="Sopas y cremas" >Sopas y cremas</option>
                                        <option value="Verduras" >Verduras</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-12 col-sm-2">
                                <div class="form-group mb-30">
                                    <select class="form-control" name="dificultad">
                                        <option value="" disabled selected hidden>Dificultad</option>
                                        <option value="">Pinche</option>
                                        <option value="">Chef</option>
                                        <option value="">Master Chef</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-sm-2">
                                <div class="form-group mb-30">
                                    <input class="form-control" type="text" name="nombre" placeholder="Ingredient o Nom" >
                                </div>
                            </div>
                            <div class="col-12 col-sm-2">
                                <div class="form-group mb-30">
                                    <button type="submit" name="buscar" class="btn bueno-btn w-100">Buscar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <br><br>
    <!-- ##### Search Area End ##### -->
    <div class="paginator">
        <div class="container">
            @foreach(array_chunk($recetas->getCollection()->all(), 3) as $row)
                <div class="row">
                    @foreach($row as $item)
                    <div class="col-12 col-sm-6 col-lg-4">
                        <div class="single-best-receipe-area mb-30">
                            <a href="{{ url('/verReceta/' . $item->id) }}"><img src="{{ asset('img/recetas/' . $item->fotografia) }}" alt=""></a>
                            <div class="receipe-content">
                                <div class="big-post-content-b">
                                    <a href="{{ url('/verReceta/' . $item->id) }}" class="post-title">{{$item->nombre}}</a>
                                    <div class="ratings">
                                    {{ date('j F, Y', strtotime($item->created_at)) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            @endforeach
            {{ $recetas->appends(Request::except('page'))->links() }}
        </div>
    </div>

    <!-- ##### Footer Area Start ##### -->
    <footer class="footer-area">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-5">
                    <!-- Copywrite Text -->
                    <p class="copywrite-text"><a href="#">
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | by <a href="https://agora.xtec.cat/ies-sabadell/" target="_blank">POGA</a>
                    </p>
                </div>
                <div class="col-12 col-sm-7">
                    <!-- Footer Nav -->
                    <div class="footer-nav">
                        <ul>
                            <li class="active"><a href="{{ url('/..') }}">Home</a></li>
                            <li><a href="#exampleModal" data-toggle="modal" data-target="#exampleModal">About Us</a></li>
                            <li><a href="https://agora.xtec.cat/ies-sabadell/">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">POGA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <div class="modal-body">
            <p>Somos un grupo de alumnos formado por<br><b>Oriol Porta, Pol Garcia e Isaac García</b>. Juntos formamos POGA, sociendad formada para realizar el proyecto final de CFGS Desenvolupaent d'aplicacions Multiplataforma.</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
    </div>
    <!-- ##### Footer Area End ##### -->

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="{{ asset('js/index_recipes/jquery/jquery-2.2.4.min.js')}}"></script>
    <!-- Popper js -->
    <script src="{{ asset('js/index_recipes/bootstrap/popper.min.js')}}"></script>
    <!-- Bootstrap js -->
    <script src="{{ asset('js/index_recipes/bootstrap/bootstrap.min.js')}}"></script>
    <!-- All Plugins js -->
    <script src="{{ asset('js/index_recipes/plugins/plugins.js')}}"></script>
    <!-- Active js -->
    <script src="{{ asset('js/index_recipes/active.js')}}"></script>
</body>
</html>