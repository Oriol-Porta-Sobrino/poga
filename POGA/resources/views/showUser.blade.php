<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Title -->
    <title>{{$usuario['name']}} - POGA Receptes &#9679; IA Recommender</title>

    <!-- Favicon -->
    <link rel="icon" href="{{ URL::asset('img/index_recipes/core-img/favicon.ico') }}">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="{{ URL::asset('css/index_recipes/styleRecipes.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css') }}">
    <!-- <link rel="stylesheet" href="{{ URL::asset('https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css') }}"> -->
</head>

<body>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="preloader-content">
            <h3>Cooking in progress..</h3>
            <div id="cooking">
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div class="bubble"></div>
                <div id="area">
                    <div id="sides">
                        <div id="pan"></div>
                        <div id="handle"></div>
                    </div>
                    <div id="pancake">
                        <div id="pastry"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Header Area Start ##### -->
    <header class="header-area">
        <!-- Top Header Area -->
        <div class="top-header-area bg-img bg-overlay">
            <div class="container h-100">
                <div class="row h-100 align-items-center justify-content-between">
                    <div class="col-12 col-sm-6">
                        <!-- Top Social Info -->
                        <div class="top-social-info">
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Pinterest"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            <a href="#" data-toggle="tooltip" data-placement="bottom" title="Linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-5 col-xl-4">
                        <!-- Top Search Area -->
                        <div class="top-search-area">
                            <form method="GET" enctype="multipart/form-data" action="{{ route('buscar') }}">
                                @csrf
                                <input type="search" name="nombre" id="topSearch" placeholder="Search">
                                <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Logo Area -->
        <div class="logo-area">
            <a href="{{ url('/..') }}"><img src="{{URL::asset('img/index_recipes/core-img/poga3.png')}}" alt=""></a>
        </div>

        <!-- Navbar Area -->
        <div class="bueno-main-menu" id="sticker">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Menu -->
                    <nav class="classy-navbar justify-content-between" id="buenoNav">

                        <!-- Toggler -->
                        <div id="toggler"><img src="{{URL::asset('img/index_recipes/core-img/toggler.png')}}" alt=""></div>

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">

                            <!-- Close Button -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                    <li><a href="{{ url('/..') }}">Home</a></li>
                                    <li><a href="#exampleModal" data-toggle="modal" data-target="#exampleModal">About Us</a></li>
                                    <li><a href="https://agora.xtec.cat/ies-sabadell/">Contact</a></li>
                                </ul>

                                <!-- Login/Register -->
                                @if( Auth::guest() )
                                <div class="login-area">
                                    <a href="{{ url('/logear') }}">Login</a> / <a href="{{ url('/registrarse') }}">Registrar</a>
                                </div>
                                @else
                                <ul class="ullogin">
                                    <li><a href="#"><i class="fa fa fa-user"></i> &nbsp;{{$usuarioSesion->name}}</a>
                                        <ul class="dropdown">
                                            <li><a href="{{ url('/verUsuario/' . $usuario->id) }}"><i class="fa fa-user"></i> &nbsp;Area Personal</a></li>
                                            <li><a href="{{ url('/insertReceta') }}"><i class="fa fa-plus"></i> &nbsp;Nueva Receta</a></li>
                                            <li class="salir"><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> &nbsp;Logout</a></li>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                                        </ul>
                                    </li>
                                </ul>
                                @endif
                            </div>
                            <!-- Nav End -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <div class="container">
        <div class="row">
            <div class="col-xl-6">
                <!-- Single Widget Area -->
                <div class="single-widget-area author-widget mb-30">
                    <div class="background-pattern bg-img">
                        <div class="author-thumbnail">
                            <img src="{{ asset('img/usuarios/' . $usuario->fotografia) }}" alt="">
                        </div>
                        <p>My name is <span>{{$usuario['name']}}</span>, I’m a passionate cook with a love food</p>
                    </div>
                    <div class="social-info">
                        <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="recipe-info">
                    <h5>Info</h5>
                    <ul class="info-data">
                        <li>
                        @if(!Auth::guest())
                            @if ($usuario->id == $usuarioSesion->id)
                            <a href="{{ url('/updateUsuarioForm/') }}"><svg class="ico-edit" style="width:24px;height:24px" viewBox="0 0 24 24">
                            <path d="M8,12H16V14H8V12M10,20H6V4H13V9H18V12.1L20,10.1V8L14,2H6A2,2 0 0,0 4,4V20A2,2 0 0,0 6,22H10V20M8,18H12.1L13,17.1V16H8V18M20.2,13C20.3,13 20.5,13.1 20.6,13.2L21.9,14.5C22.1,14.7 22.1,15.1 21.9,15.3L20.9,16.3L18.8,14.2L19.8,13.2C19.9,13.1 20,13 20.2,13M20.2,16.9L14.1,23H12V20.9L18.1,14.8L20.2,16.9Z" />
                            </svg></a></span>
                            @else
                            <span>
                            @endif
                        @endif
                        </span></li>
                    </ul>

                    <ul class="info-data">
                        <li>Nombre:<span>{{$usuario['name']}}</span></li>
                        <li>Apellidos:<span>{{$usuario['apellido']}}</span></li>
                        <li>Genero:<span>{{$usuario['genero']}}</span></li>
                        <li>F.Nacimiento:<span>{{ date('j F, Y', strtotime($usuario->nacimiento)) }}</span></li>
                        <li>Pais:<span>{{$usuario['pais']}}</span></li>
                        <li>Ciudad:<span>{{$usuario['ciudad']}}</span></li>
                        @if(!Auth::guest())
                            @if ($usuario->id == $usuarioSesion->id)
                            <li>Telf:<span>{{$usuario['telefono']}}</span></li>
                            <li>E-mail:<span>{{$usuario['email']}}</span></li>
                            @endif
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="card col-xl-12 bg-light text-dark">
                <div class="card-body">
                    <h6 class="display-3 text-center title">Mis Recetas</h6>
                    <div class="table-responsive">
                        <table class="table table-striped table-hover ">
                            <thead class="thead-color">
                                <tr>
                                    <th>Foto</th>
                                    <th>Titulo Receta</th>
                                    <th>Tipo</th>
                                    <th>Origen</th>
                                    <th>Dificultad</th>
                                    <th>Tiempo</th>
                                    <th>Comensales</th>
                                    <th>Última Modificación</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($recetas as $item)
                                <tr>
                                    <td>
                                        <div class="foto-tabla">
                                            <a href="{{ asset('img/recetas/' . $item->fotografia) }}" class="img-zoom" title="{{$item->nombre}}">
                                            <img src="{{ asset('img/recetas/' . $item->fotografia) }}" /></a>
                                        </div>
                                    </td>
                                    <th scope="row">{{$item->nombre}} </th>
                                    <td>{{$item->tipo}}</td>
                                    <td>{{$item->origen}}</td>
                                    <td>{{$item->dificultad}}</td>
                                    <td>{{$item->duracion}}</td>
                                    <td>{{$item->personas}}</td>
                                    <td> {{ date('j F, Y', strtotime($item->created_at)) }}</td>
                                    <td>@if(!Auth::guest())
                                             @if ($usuario->id == $usuarioSesion->id)
                                             <a href="{{ url('/updateRecetaForm/' . $item->id) }}"><svg class="ico-edit" style="width:24px;height:24px" viewBox="0 0 24 24">
                                            <path d="M8,12H16V14H8V12M10,20H6V4H13V9H18V12.1L20,10.1V8L14,2H6A2,2 0 0,0 4,4V20A2,2 0 0,0 6,22H10V20M8,18H12.1L13,17.1V16H8V18M20.2,13C20.3,13 20.5,13.1 20.6,13.2L21.9,14.5C22.1,14.7 22.1,15.1 21.9,15.3L20.9,16.3L18.8,14.2L19.8,13.2C19.9,13.1 20,13 20.2,13M20.2,16.9L14.1,23H12V20.9L18.1,14.8L20.2,16.9Z" /></svg></a>
                                            @endif
                                        @endif
                                        <span><a href="{{ url('/verReceta/' . $item->id) }}"><i class="fa fa-eye"></i></a></span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="d-flex justify-content-center">
                        {!! $recetas->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Footer Area Start ##### -->
    <footer class="footer-area">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-5">
                    <!-- Copywrite Text -->
                    <p class="copywrite-text"><a href="#">
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | by <a href="https://agora.xtec.cat/ies-sabadell/" target="_blank">POGA</a>
                    </p>
                </div>
                <div class="col-12 col-sm-7">
                    <!-- Footer Nav -->
                    <div class="footer-nav">
                        <ul>
                            <li class="active"><a href="{{ url('/..') }}">Home</a></li>
                            <li><a href="#" data-toggle="modal" data-target="#exampleModal">About Us</a></li>
                            <li><a href="https://agora.xtec.cat/ies-sabadell/">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">POGA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <div class="modal-body">
            <p>Somos un grupo de alumnos formado por<br><b>Oriol Porta, Pol Garcia e Isaac García</b>. Juntos formamos POGA, sociendad formada para realizar el proyecto final de CFGS Desenvolupaent d'aplicacions Multiplataforma.</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
    </div>
    <!-- ##### Footer Area End ##### -->
    <script src="{{URL::asset('https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js')}}"></script>
    <script src="{{ asset('js/index_recipes/jquery/jquery-2.2.4.min.js')}}"></script>
    <!-- Popper js -->
    <script src="{{ asset('js/index_recipes/bootstrap/popper.min.js')}}"></script>
    <!-- Bootstrap js -->
    <script src="{{ asset('js/index_recipes/bootstrap/bootstrap.min.js')}}"></script>
    <!-- All Plugins js -->
    <script src="{{ asset('js/index_recipes/plugins/plugins.js')}}"></script>
    <!-- Active js -->
    <script src="{{ asset('js/index_recipes/active.js')}}"></script>
</body>
</html>